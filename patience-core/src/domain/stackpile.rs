//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::vec::*;

use super::KnownTopStack;
use super::Suggestionable;
use super::CardStack;
use super::TopMoveable;
use super::card::*;

#[derive(PartialEq,Debug)]
pub struct StackPile {
    pub cards: Vec<Card>
}

impl StackPile {

    pub fn new() -> StackPile {
        StackPile {
            cards: Vec::new()
        }
    }

    pub fn is_finished(&self) -> bool {
        self.cards.len() == KING as usize
    }
}

impl KnownTopStack for StackPile {
    fn top(&self) -> Option<&Card> {
        self.cards.last()
    }
}

impl CardStack for StackPile {
    fn push(&mut self, card: Card) -> Result<(), (Card, Option<&str>)> {

        if card.is_unknown() {
            return Err((card, None));
        }

        if self.cards.is_empty() && card.value != ACE {
            return Err((card, None));
        }

        if self.can_suggest(&card) {
            self.cards.push(card);
            return Ok(())
        }

        Err((card, None))
    }
}

impl Suggestionable for StackPile {
    fn can_suggest(&self, card: &Card) -> bool {

        match self.top() {
            None => card.value == ACE,
            Some(top) => top.value + 1 == card.value && top.suit == card.suit
        }
    }
}

impl TopMoveable for StackPile {
    fn move_top_to<CS: CardStack>(&mut self, card_stack: &mut CS) -> Result<(), Option<&str>> {
        match self.cards.pop() {
            Some(card) => {
                match card_stack.push(card) {
                    Ok(_) => Ok(()),
                    Err((bc, _f)) => {
                        self.cards.push(bc);
                        Err(None)
                    }
                }
            },
            None => {
                Err(None)
            }
        }
    }
}

#[cfg(test)]
pub mod tests {
    use crate::domain::stackpile::*;

    #[test]
    fn stackpile_is_finished() {
        let mut stackpile = StackPile::new();
        for i in ACE..=KING {
            let card: Card = Card::create_known(i, Suit::Spade);
            let _ = stackpile.push(card);
        }

        assert!(stackpile.is_finished());
    }

    #[test]
    fn rejected_move_to_unknown_stackpile() {
        let card = Card::create_unknown(ACE, Suit::Spade);
        let mut stackpile = StackPile::new();

        let result = stackpile.push(card);

        assert!(result.is_err());

    }

    #[test]
    fn accepted_move_to_known_ace_stackpile() {
        let card = Card::create_known(ACE, Suit::Spade);
        let mut stackpile = StackPile::new();

        let result = stackpile.push(card);

        assert!(result.is_ok());

    }

    #[test]
    fn rejected_move_to_of_known_2_to_empty_stackpile() {
        let card = Card::create_unknown(2, Suit::Spade);
        let mut stackpile = StackPile::new();

        let result = stackpile.push(card);

        assert!(result.is_err());

    }

    #[test]
    fn accepted_move_to_of_known_2_to_stackpile_with_ace() {
        let ace: Card = Card::create_known(ACE, Suit::Spade);
        let mut stackpile = StackPile::new();
        let _ = stackpile.push(ace);

        let two: Card = Card::create_known(2, Suit::Spade);
        let result = stackpile.push(two);

        assert!(result.is_ok());
    }

    #[test]
    fn can_suggest_ace_to_empty_stackpile() {
        // Given
        let stackpile = &mut StackPile::new();
        let ace: Card = Card::create_known(ACE, Suit::Spade);

        // When
        let result = stackpile.can_suggest(&ace);

        // Then
        assert_eq!(result, true);
    }

    #[test]
    fn can_suggest_2_to_empty_stackpile() {
        // Given
        let stackpile = &mut StackPile::new();
        let two: Card = Card::create_known(2, Suit::Spade);

        // When
        let result = stackpile.can_suggest(&two);

        // Then
        assert_eq!(result, false);
    }

    #[test]
    fn can_suggest_heart_2_to_stackpile_with_heart_ace() {
        // Given
        let stackpile = &mut StackPile::new();
        let ace: Card = Card::create_known(ACE, Suit::Heart);
        let _ = stackpile.push(ace);
        let two: Card = Card::create_known(2, Suit::Heart);

        // When
        let result = stackpile.can_suggest(&two);

        // Then
        assert_eq!(result, true);
    }

    #[test]
    fn can_suggest_heart_2_to_stackpile_with_diamond_ace() {
        // Given
        let stackpile = &mut StackPile::new();
        let ace: Card = Card::create_known(ACE, Suit::Heart);
        let _ = stackpile.push(ace);
        let two: Card = Card::create_known(2, Suit::Diamond);

        // When
        let result = stackpile.can_suggest(&two);

        // Then
        assert_eq!(result, false);
    }
}