#!/usr/bin/env bash
pdir=$(pwd)

cd "${pdir}/patience-core"
echo "remove built artefacts from 'patience-core'"
cargo clean
cd "${pdir}/wasm-core-binding"
echo "remove built artefacts from 'wasm-core-binding'"
cargo clean

cd "${pdir}/c-core-binding"
./clean.sh

cd "${pdir}/patience-web"
./clean.sh "$(pwd)"

cd "${pdir}/swift-c-binding"
./clean.sh

cd "${pdir}"

echo "remove built artefacts from base"
cargo clean
