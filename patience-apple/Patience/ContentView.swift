// Rust Patience Game, Swift Frontend
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI
import PatienceFoundation

struct ContentView: View {
    
    @State private var state: Patience
    
    var body: some View {
        Playfield().environment(PatienceModel(patience: self.state))
    }
    
    init() {
        self.state = try! Patience()
    }
}
#Preview {
    ContentView()
}
