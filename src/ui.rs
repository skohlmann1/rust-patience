//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use patience_core::application::*;
use regex::Regex;

pub mod terminal;


/**
ABNF for the move command syntax

``` ABNF 
move = talonMove / indexMove / topMove ; optional later with " / help / [ offer / possibilities ]"
talonMove = discardPile
indexMove = column *WSP index *WSP column
topMove = ( discardPile / column / stackpile) *WSP ( column / stackpile )
discardPile = "d" "p"
column = "c" %x31-37 ; 1-7
index = single / higher
single = %x31-39 ; 1-9
higher = "1" DIGIT 
stackpile = *WSP [ club / heart / spade / diamond ] *WSP
club = "c"
diamond = "d"
spade = "s"
heart = "h"
```

Regexp to validate a move command[^note]:

```regex
^([Dd][Pp]|[Cc][1-7][ \t]*([1-9]|1[0-9])[ \t]*[Cc][1-7]|(([Dd][Pp]|[Cc][1-7]|([ \t]*[CcHhSsDd]?[ \t]*))[ \t]*([Cc][1-7]|([ \t]*[CcHhSsDd]?[ \t]*))))$
```

[^note] Generated with [ABNF to REGEX Library](https://github.com/michaelrsweet/abnf) by Michael R Sweet
*/
pub fn parse_command<'a>(command: &'a str) -> Result<MoveCommand, Option<&str>> {

    let re_command = Regex::new(r"(?m)^(([Pp]|([Dd][Pp])?)|[Cc][1-7][ \t]*([1-9]|1[0-9])[ \t]*[Cc][1-7]|((([Pp]|([Dd][Pp])?)|[Cc][1-7]|([ \t]*[CcHhSsDd]?[ \t]*))[ \t]*([Cc][1-7]|([ \t]*[CcHhSsDd]?[ \t]*))))$").unwrap();
    let re_discardpile = Regex::new(r"^[Dd][Pp]$").unwrap();
    let re_stackpile = Regex::new(r"^([ \t]*([CcHhSsDd])?[ \t]*)$").unwrap();
    let re_index = Regex::new(r"^([1-9]|1[0-9])$").unwrap();
    let re_column = Regex::new(r"^[Cc][1-7]$").unwrap();
    let re_column_start = Regex::new(r"^[Cc][1-7]").unwrap();
    let re_column_end = Regex::new(r"[Cc][1-7]$").unwrap();

    if command.is_empty() || !re_command.is_match(command) {
        return Err(None)
    }
    
    let command: &str = command.trim();
    let result = re_command.captures_iter(command);

    let mut move_command = MoveCommand::builder("");
    let mut first: &str = "";
    
    for mat in result {
        for cap in mat.iter() {

            if let Some(c) = cap {
                let part = c.as_str();

                if first.is_empty() {
                    first = part;
                }

                if re_index.is_match(part) {
                    if re_column_start.is_match(first) {
                        move_command.with_index(part.parse().unwrap()).with_start(first.get(0..2).unwrap());
                    }
                    if re_column_end.is_match(first) {
                        let target = first.get((first.len() - 2)..first.len()).unwrap();
                        move_command.with_target(target);
                    }
                }

                if re_discardpile.is_match(part) {
                    if move_command.is_empty() {
                        move_command.with_start(part);
                    }
                }
                if re_stackpile.is_match(part) {
                    if move_command.is_empty() {
                        move_command.with_start(part);
                    } else {
                        move_command.with_target(part);
                    }
                }
                if re_column.is_match(part) {
                    if move_command.is_empty() {
                        move_command.with_start(part);
                    } else {
                        move_command.with_target(part);
                    }
                }
            }
        }
    }

    match move_command.is_empty() {
        true => Err(None),
        false => Ok(move_command)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_index_command() {
        let cmd = "c12c3";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.index, Some(2));
                assert_eq!(mov.start, "c1");
                assert_eq!(mov.target, Some("c3"));
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_single_dicardpile_command() {
        let cmd = "dp";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "dp");
                assert_eq!(mov.target, None);
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_dicardpile_to_column_command() {
        let cmd = "dpc1";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "dp");
                assert_eq!(mov.target, Some("c1"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_dicardpile_to_spade_stackpile_command() {
        let cmd = "dps";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "dp");
                assert_eq!(mov.target, Some("s"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_dicardpile_to_club_stackpile_command() {
        let cmd = "dpc";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "dp");
                assert_eq!(mov.target, Some("c"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_dicardpile_to_diamond_stackpile_command() {
        let cmd = "dpd";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "dp");
                assert_eq!(mov.target, Some("d"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_dicardpile_to_heart_stackpile_command() {
        let cmd = "dph";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "dp");
                assert_eq!(mov.target, Some("h"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    // Note this is allowed by the ABNF yet but not by the Game later.
    #[test]
    fn parse_stackpile_to_stackpile_command() {
        let cmd = "cc";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "c");
                assert_eq!(mov.target, Some("c"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_stackpile_to_column_command() {
        let cmd = "cc1";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "c");
                assert_eq!(mov.target, Some("c1"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_column1_to_column2_command() {
        let cmd = "c1 c2";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "c1");
                assert_eq!(mov.target, Some("c2"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    // Such a move doesn't make any sense but ist allowed by the game.
    #[test]
    fn parse_column1_to_column1_command() {
        let cmd = "c1 C1";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "c1");
                assert_eq!(mov.target, Some("C1"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    #[test]
    fn parse_column_to_stackpile_command() {
        let cmd = "c1 H";
        match parse_command(cmd) {
            Ok(mov) => {
                assert_eq!(mov.start, "c1");
                assert_eq!(mov.target, Some("H"));
                assert_eq!(mov.index, None);
            },
            Err(_) => assert!(false)
        }
    }

    // I don't wanna test als permutations including the case sensitive parts.
    // So now some illegal commands
    #[test]
    fn failing_parse_column_to_discardpile_command() {
        let cmd = "c1 dp";
        assert!(parse_command(cmd).is_err());
    }

    #[test]
    fn failing_parse_spade_stackpile_to_discardpile_command() {
        let cmd = "s dp";
        assert!(parse_command(cmd).is_err());
    }
}
