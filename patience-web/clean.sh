#!/usr/bin/env bash

set -o nounset           

target="${1}"

echo "remove built artefacts from '${PWD##*/}'"
rm -rf "${target}/pkg"
rm -rf "${target}/static"
