/*
 *  Rust Patience Game, Web edition
 *  Copyright (C) 2024 Sascha Kohlmann
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Contains the visible representation of a playcard with an image.
 * 
 * The element is templatable by lookup a `play-card-template` <template>.
 * If no template is available a standard template is used.
 * 
 * The image _slot_ name is `play-card-image`.
 * 
 * Addtional the <play-card> element contains the two attributes
 * 
 * - `card-type`
 * - `card-index`
 * 
 * `card-type` can have the values
 * 
 * - `none` for placeholder card
 * - `known` for an known card
 * - `unknown` for an unknown card
 *
 * `card-index` is available if and only if `card-type` is not `none`.
 * It represents the index of the card within a stack of cards starting with 0.
 */
export default class PlayCard extends HTMLElement {

    static #playCardSlotName = "play-card-image";
    static #playCardTemplateName = "play-card-template";

    #root;

    constructor() {
        super();
        this.#root = this.attachShadow({ mode: 'closed' });
        this.#html(this.#root);
    }

    #html(root) {
        const template = document.getElementById(PlayCard.#playCardTemplateName);
        if (template !== null) {
            root.appendChild(template.content.cloneNode(true));
        } else {
            root.innerHTML = `<img slot="${PlayCard.#playCardSlotName}" />`;
        }
    }

    /**
     * @param {Image} imageNode
     */
    set image(imageNode) {
        this.#setImage(this.#root, imageNode);
    }

    #setImage(node, imageNode) {
        if (imageNode.slot !== undefined && imageNode.slot.trim() === PlayCard.#playCardSlotName) {
            node.childNodes.forEach(childNode => {
                if (childNode.slot === PlayCard.#playCardSlotName) {
                    node.replaceChild(imageNode, childNode);
                } else {
                    this.#setImage(childNode, imageNode);
                }
            });
        }
    }

    static get playCardSlotName() {
        return this.#playCardSlotName;
    }

    static get playCardTemplateName() {
        return this.#playCardTemplateName;
    }

    connectedCallback() { 

    }
}

if (!customElements.get('play-card')) {
    customElements.define('play-card', PlayCard);
}
