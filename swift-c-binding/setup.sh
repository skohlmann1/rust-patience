#!/usr/bin/env bash
#
# Prepare environment to build the project.

set +o nounset

target=debug
if [[ "${1}" == "release" ]]; then
    target="${1}"
fi

cd ../c-core-binding
./build.xcframework.sh "${target}"
cd -
