// Rust Patience Game, Swift Frontend
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import SwiftUI
import PatienceFoundation


struct StackFoundation {
    let card: Card
    let paddingPercentage: Double
    let knownOverlayPercentage: Double
    let unknownOverlayPercentage: Double
    
    var cardHeight: Double { get {
            return self.card.width * self.card.ratio
        }
    }
    
    var cardWidth: Double { get {
            return self.card.width
        }
    }

    var padding: Double { get {
            return self.card.width - self.card.width / (100 + self.paddingPercentage) * 100
        }
    }

    init(card: Card,
         paddingPercentage: Double = 5,
         knownOverlayPercentage: Double = 25,
         unknownOverlayPercentage: Double = 10) {
        self.card = card
        self.paddingPercentage = paddingPercentage
        self.knownOverlayPercentage = knownOverlayPercentage
        self.unknownOverlayPercentage = unknownOverlayPercentage
    }
}

struct CardStack : View {
    
    let stackName: String
    let emptyStack: SpecialCard
    var foundation: StackFoundation
    var cards: [CardSymbols]
    
    var body: some View {
        ZStack {
            if cards.isEmpty || cards[0].isKnown() {
                cardView(special: emptyStack).frame(width: foundation.cardWidth, height: foundation.cardHeight)
            }
            ForEach(0..<cards.count) { i in
                CardView(index: i + 1, symbols: cards[i])
                    .offset(calculateOffset(index: i))
                    .onTapGesture {
                        debugPrint("Sow something")
                        if cards[i].isKnown() {
                            debugPrint("\(cards[i].cardName())@\(i)")
                        }
                    }
            }
        }
        .frame(width: foundation.cardWidth,
               height: calculateFrameHeight(),
               alignment: .topLeading)
    }
    
    private func calculateOffset(index: Int) -> CGSize {
        var offset = 0.0
        for i in 0..<index {
            offset += foundation.cardHeight / 100 * calculateOverlayPercentage(i)
        }
        
        return CGSize(width: 0, height: offset)
    }
    
    private func calculateOverlayPercentage(_ index: Int) -> CGFloat {
        if cards[index].isKnown() {
            let predecessorIndex = index
            if predecessorIndex < 0 || cards[predecessorIndex].isKnown() {
                return foundation.knownOverlayPercentage
            }
        }
        return foundation.unknownOverlayPercentage
    }

    private func calculateFrameHeight() -> CGFloat {
        return cards.count <= 1 ? foundation.cardHeight : foundation.cardHeight + calculateOffset(index: cards.count - 1).height
    }
}


 #Preview {
 let symbolsEmpty: [CardSymbols] = []
 
 let symbolsLarge = [
 CardSymbols(symbol: "a", suit: nil, value: nil),
 CardSymbols(symbol: "a", suit: nil, value: nil),
 CardSymbols(symbol: "a", suit: "♥", value: 4),
 CardSymbols(symbol: "b", suit: "♣", value: 3),
 CardSymbols(symbol: "c", suit: "♦", value: 2),
 CardSymbols(symbol: "d", suit: "♠", value: 1)
 ]
 
 let symbolsOne = [
 CardSymbols(symbol: "d", suit: "♠", value: 1)
 ]
 
 let symbolsTwoKnown = [
 CardSymbols(symbol: "d", suit: "♦", value: 2),
 CardSymbols(symbol: "d", suit: "♠", value: 1)
 ]
 
 let symbolsTwoWithUnkown = [
 CardSymbols(symbol: "d", suit: nil, value: nil),
 CardSymbols(symbol: "d", suit: "♠", value: 1)
 ]
 
 let symbolsUnknown = [
 CardSymbols(symbol: "d", suit: nil, value: nil),
 ]
 
 let basic = StackFoundation(card: Card(width: 200))
 
 return CardStack(stackName: "dummy", emptyStack: .add, foundation: basic, cards: symbolsLarge)
 }
 
