//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! C wrapper for [application::Game](patience_core::application::Game)

extern crate cbindgen;

use std::env;

const HEADER_NAME: &str = "libc_patience_core_binding.h";
const TARGET_DIR: &str  = "target";

fn main() {
    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    cbindgen::Builder::new()
        .with_crate(crate_dir)
        .with_language(cbindgen::Language::C)
        .with_documentation(true)
        .with_std_types(true)
        .with_after_include("\ntypedef struct Game { } Game;")
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file(format!("./{TARGET_DIR}/{HEADER_NAME}"));

    copy_header_file();

    // println!("cargo:warning=Out dir: {}", env::var_os("OUT_DIR").unwrap().to_str().unwrap());
}

fn copy_header_file() {
    let target = env::var_os("TARGET").unwrap();
    let profile = env::var_os("PROFILE").unwrap();
    if let Ok(path) = env::current_dir() {
        let outpath = path.as_path();
        let input_path = path.as_path().join(TARGET_DIR).join(HEADER_NAME);
        let mut output_path = outpath.parent().unwrap().join(TARGET_DIR).join(target.clone());
        if output_path.exists() {
            output_path = output_path.join(profile).join(HEADER_NAME);
        } else {
            output_path = path.as_path().parent().unwrap().join(TARGET_DIR).join(profile).join(HEADER_NAME);
        }
        if let Err(err) = std::fs::copy(input_path, output_path) {
            println!("cargo:warning={:?}", err);
        };
    }
}