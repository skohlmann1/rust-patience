// Rust Patience Game, Swift Frontend
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


import PatienceFoundation
import SwiftUI
import Observation


@Observable final class PatienceModel {
    
    @ObservationIgnored private var patience: Patience
    
    var column1: [CardSymbols]
    var column2: [CardSymbols]
    var column3: [CardSymbols]
    var column4: [CardSymbols]
    var column5: [CardSymbols]
    var column6: [CardSymbols]
    var column7: [CardSymbols]
    var column8: [CardSymbols]
    
    var discardPile: [CardSymbols]
    
    var suit1: [CardSymbols]
    var suit2: [CardSymbols]
    var suit3: [CardSymbols]
    var suit4: [CardSymbols]
    
    var msg: String = ""

    init(patience: Patience) {
        self.patience = patience
        self.column1 = patience.queryColumn(ColumnQuery(columnIndex: 1))
        self.column2 = patience.queryColumn(ColumnQuery(columnIndex: 2))
        self.column3 = patience.queryColumn(ColumnQuery(columnIndex: 3))
        self.column4 = patience.queryColumn(ColumnQuery(columnIndex: 4))
        self.column5 = patience.queryColumn(ColumnQuery(columnIndex: 5))
        self.column6 = patience.queryColumn(ColumnQuery(columnIndex: 6))
        self.column7 = patience.queryColumn(ColumnQuery(columnIndex: 7))
        self.column8 = patience.queryColumn(ColumnQuery(columnIndex: 8))
        
        self.discardPile = patience.queryDiscardPile(DiscardPileQuery(all: true))

        self.suit1 = []
        self.suit2 = []
        self.suit3 = []
        self.suit4 = []
    }
    
    func doMove(start: String, index: UInt32?, target: String?) {
        let moveCmd = MoveCommand(start: start, index: index, target: target)
        let _ = self.patience.doMove(moveCmd)
        self.discardPile = self.patience.queryDiscardPile(DiscardPileQuery(all: true))
        self.msg = "performed: \(moveCmd) - \(self.discardPile.count)"
    }
}
