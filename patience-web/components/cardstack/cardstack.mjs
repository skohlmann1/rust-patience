/*
 *  Rust Patience Game, Web edition
 *  Copyright (C) 2024 Sascha Kohlmann
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import UriTemplate from "../UriTemplate/uri-templates.mjs";
import PlayCard from "../playcard/playcard.mjs";

export default class CardStack extends HTMLElement {

    constructor() { super(); }

    get topOnly() {
        const to = this.getAttribute('top-only');
        return to !== undefined && to.trim() === "true";
    }

    /**
     * A URI template following RFC 6570.
     * The URI must contains a field "imagename" for the [Playcard#shortName()].
     * 
     * E.g. http://example.com/card-images/{imagename}.svg"
     */
    get cardTemplateUrl() {
        return this.getAttribute('card-template-url');
    }

    /**
     * The back card image URL for e.g. [Card#isKnown()].
     * E.g. http://example.com//back.svg"
     */
    get backCardUrl() {
        return this.getAttribute('back-card-url');
    }

    /**
     * Card URL for no available card.
     * E.g. http://example.com//empty.svg"
     */
    get emptyCardUrl() {
        return this.getAttribute('empty-card-url');
    }

    connectedCallback() {
        if (this.cardTemplateUrl === null || this.cardTemplateUrl === undefined || this.cardTemplateUrl.trim() == "") {
            throw new Error("card-template-url must not be empty and a template URL (RFC 6570)");
        }
        this.updateCards([]);
    }

    /**
     * @param {Card[]} cards
     */
    updateCards(cards) {
        if (this.isConnected) {
            this.#removeAllChildren();
            if (cards === undefined || cards === null || cards.length === 0) {
                let playcard = this.#buildPlayCard({ uri: this.emptyCardUrl, name: "", type: "none" });
                this.#appendTo(playcard);
            } else {
                this.#adjustCards(cards).forEach((item, index) => {
                    if (item.isKnown()) {
                        let imageUri = new UriTemplate(this.cardTemplateUrl).fill({ imagename: item.shortName });
                        let playcard = this.#buildPlayCard({uri: imageUri, name: item.name, index: index, type: "known" });
                        this.#appendTo(playcard);
                    } else {
                        let playcard = this.#buildPlayCard({ uri: this.backCardUrl, name: "Unknown", index: index, type: "unknown" });
                        this.#appendTo(playcard);
                    }
                });
            }
        }
    }

    #buildPlayCard(imageAttributes) {
        let image = this.#createImageElement(imageAttributes.uri, imageAttributes.name);
        let playcard = new PlayCard();
        if (imageAttributes.index !== undefined && imageAttributes.index !== null) {
            playcard.setAttribute("card-index", imageAttributes.index);
        }
        playcard.setAttribute("card-type", imageAttributes.type);
        playcard.setAttribute("name", imageAttributes.name);
        playcard.image = image;
        return playcard;
    }

    #adjustCards(cards) {
        let retval = cards;
        if (cards.constructor !== Array) {
            retval = [cards];
        }
        if (this.topOnly) {
            retval = [retval[retval.length - 1]];
        }
        return retval;
    }

    #removeAllChildren() {
        while (this.firstChild) {
            this.removeChild(this.lastChild);
        }
    }

    #appendTo(child) {
        this.appendChild(child);
    }

    #createImageElement(imageUrl, imageTitle) {
        const image = new Image();
        image.src = imageUrl;
        image.title = imageTitle;
        image.slot = "play-card-image";
        image.setAttribute("width", "100%");
        return image;
    }
}

if (!customElements.get('card-stack')) {
    customElements.define('card-stack', CardStack);
}

export class Card {

    #value;
    #suitSymbol;

    /**
     * @param {value} value of the card. Must be between 1 and 13.
     * @param {suit} suit symbol of the card. Must be either ♦ (diamond) or ♥ (heart) or ♠ (spade) or ♣ (club)
     * @constructor
     */
    constructor(value, suit) {
        if (suit !== undefined && suit !== "♦" && suit !== "♥" && suit !== "♠" & suit !== "♣") {
            throw new Error("Unsupported suit " + suit);
        }
        this.#suitSymbol = suit;
        if (value !== undefined && (value < 1 || suit > 13)) {
            throw new Error("Unsupported value " + suit + " Must be between 1 and 13");
        }
        this.#value = value;
        this.#suitSymbol = suit;
    }

    /**
     * @returns {boolean} true if and only if the value if not undefined.
     */
    isKnown() {
        return this.#value !== undefined && this.#suitSymbol !== undefined
                && this.#value !== null && this.#suitSymbol !== null;
    }

    /**
     * ASCII based abbreviation of the suit symbol.
     * 
     * - "d" for "♦"
     * - "h" for "♥"
     * - "s" for "♠"
     * - "c" for "♣"
     * 
     * @return {String} ASCII based abbreviation of the suit symbol
     * @readonly
     */
    get suitCharacter() {

        switch (this.#suitSymbol) {
            case "♦": return "d";
            case "♥": return "h";
            case "♠": return "s";
            case "♣": return "c";
            default: throw new Error(`Unknown suit symbol ${this.#suitSymbol}`);
        }
    }

    /**
     * A human readable name of the card. Currently english only.
     * E.g. value = "1" and suite = "♠" is "Ace of Spades".
     * 
     * If an only if {@link Card#isKnown} returns "false" the name if "Unknown".
     * 
     * @return {String} a human readable card name.
     * @readonly
     */
    get name() {
        if (!this.isKnown()) {
            return "Unknown";
        }
        let _type = undefined;
        switch (this.#value) {
            case 1: _type = "Ace"; break;
            case 11: _type = "Jack"; break;
            case 12: _type = "Queen"; break;
            case 13: _type = "King"; break;
            default: _type = "" + this.#value; break;
        }

        let _suit = undefined;
        switch (this.#suitSymbol) {
            case "♦": _suit = "Diamonds"; break;
            case "♥": _suit = "Hearts"; break;
            case "♠": _suit = "Spades"; break;
            case "♣": _suit = "Clubs"; break;
            default: throw new Error(`Unknown suit symbol ${this.#suitSymbol}`);
        }

        return `${_type} of ${_suit}`;
    }

    /**
     * Get the short name of the card wether is of two or three characters or "unknown".
     * The first part is the {@link Card#suitCharacter} the second part is the value of the card where
     * 
     * - "1" is replaced with "a" for ace.
     * - "11" is replaced with "j" for jack.
     * - "12" is replaced with "q" for queen.
     * - "13" is replaced with "k" for king.
     * 
     * NOTE: as I'm from Germany I don't name cards "sa" or "hj", because of him.
     * So such short names are replaced with
     * 
     * - "lemmy" for "sa".
     * - "zaphod" for "hj".
     * 
     * If and only if {@link Card#isKnown} returns false, the name is "unknown".
     * 
     * @return {String} the short name of the card.
     * @readonly
     */
    get shortName() {
        if (!this.isKnown()) {
            return "unknown";
        }
        let cv = undefined;
        switch (this.#value) {
            case 1: cv = "a"; break;
            case 11: cv = "j"; break;
            case 12: cv = "q"; break;
            case 13: cv = "k"; break;
            default: cv = "" + this.#value; break;
        }
        let cardBaseName = this.suitCharacter + cv;
        if (cardBaseName == "sa") {
            return "lemmy";
        }
        if (cardBaseName == "hj") {
            return "zaphod";
        }
        return cardBaseName;
    }

    get [Symbol.toStringTag]() {
        return this.cardName;
    }
}

