// Rust Patience Game, C Binding for Swift
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import XCTest

@testable import PatienceFoundation

final class PatienceTests: XCTestCase {
    
    func testTalonAfterInit() throws {
        // Given
        let game = try! Patience()
        
        // When
        let talonSize = game.talon()
        
        // Then
        XCTAssertEqual(talonSize, 24)
    }
    
    func testIsSolvableAfterInit() throws {
        // Given
        let game = try! Patience()
        
        // Then
        XCTAssert(!game.isSolvable())
    }
    
    func testIsFinishedAfterInit() throws {
        // Given
        let game = try! Patience()
        
        // Then
        XCTAssert(!game.isFinished())
    }
    
    func testColumnIndices() {
        // Given
        let game = try! Patience()
        
        // When
        let indices = game.columnIndices();
        
        // Then
        XCTAssertEqual(indices.count, 7)
        XCTAssertEqual(indices.first!, 1)
        XCTAssertEqual(indices.last!, 7)
    }
    
    func testDiscardPileAfterDoMoveFor() {
        // Given
        let game = try! Patience()
        let _ = game.doMove(MoveCommand(start: "dp", index: nil, target: nil))
        
        // When
        let cardSymbols = game.queryDiscardPile()
        
        // Then
        XCTAssertEqual(cardSymbols.count, 1)
        XCTAssertNotNil(cardSymbols[0].value)
        XCTAssertNotNil(cardSymbols[0].suit)
    }
    
    func testSecondColumn() {
        // Given
        let game = try! Patience()
        
        // When
        let columnCards = game.queryColumn(ColumnQuery(columnIndex: 2))
        
        // Then
        XCTAssertEqual(columnCards.count, 2)
        XCTAssert(!columnCards.first!.isKnown())
        XCTAssert(columnCards.last!.isKnown())
    }
}
