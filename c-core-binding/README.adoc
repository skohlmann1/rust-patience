= C core binding
:source-highlighter: rouge
:link-wasm: https://webassembly.org/
:toc: preamble 
:link-rust_pointer: https://doc.rust-lang.org/std/primitive.pointer.html

This crate contains a small adapter of the Patience application to the C world.

== Build

Run `cargo build`

The header file is located in `target` with name `patience_core_binding.h`.

=== Build as Apple `XCFramework` bundle

The `patience-swift` package requires an XCFramework of the C library.
To build such a framework `xcodebuild` is required.
`xcodebuild` is part of Apple's `xcode` development environment.

If `xcodebuild` is available, please execute

  ./build.xcframework.sh

The script creates an XCFramework with yet an iOS and a MacOS aarch64 C library and header files.
The result is stored in the directory `./target/RustPatience.xcframework` and as zipped file also in `./target/` 

NOTE: `./build.xcframework.sh` is also called from `./swift-c-binding/setup.sh` of the repository root directory.


See also <<l20240708,Learnings from 2024-07-08>>.

=== Dependencies

- link:../patience-core/[`patience-core`]

== Learnings

[[l20240708]]
=== 2024-07-08 - Problems with `xcodebuild`

If Xcode is installed, an error message similar to the following may still be displayed:

  error: failed to get iphonesimulator SDK path: process exit with error: xcrun: error: SDK "iphonesimulator" cannot be located
       xcrun: error: SDK "iphonesimulator" cannot be located
       xcrun: error: unable to lookup item 'Path' in SDK 'iphonesimulator'

In this case, the `xcodebuild` tool cannot be accessed correctly.
You can check this by calling xcodebuild from the command line.
The error message then looks something like this:

  xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

In this case, you can repair the behaviour by calling the following command:

  > sudo xcode-select -s ~/Applications/Xcode.app/Contents/Developer

Then call `./clean.sh` and `./build.xcframework.sh` again.


=== 2024-04-19

Handling arrays in C is tricky when using from e.g. Swift.
Therefor I created functions to return an array value from a given index.
This creates a little bit function call overhead but it's easier for the client.

=== 2024-04-17

First trial to create a new game inside of Swift failed.

Reason: the function `patience_create_game` returns a pointer to `Game`.
However, `Game` is not further defined in the header file.
I solved the problem by injecting an artificial `Game` `struct` into the header file using `cbindgen`.
See xref:build.rs[`build.rs`] for more information.

=== 2024-04-15

Some parts of the implementation were more difficult than expected.
For example the return value of an array of `CardSymbols`.
See `patience_query_discardpile` function for example.

Also handling `null` is more complex with a language supporting such a behavior :-)

== Howto

One challenge with Rust's Foreign Function Interface (FFI) is the simplicity that is suggested.
Add `pub extern "C"` and take `CString` and `CStr`.
Spice it up with some `c_unint` and the integration is ready.
This is correct for trivial interfaces.
For more complicated interfaces, you will hardly find any documentation as a beginner.
The FFI is certainly not a functionality of Rust for beginners.

In this section I describe some solutions for more complicated use cases.
Even a `Vec<CardSymbols>` as a return value is no longer trivial.

=== How to handle `null`

C knows `null`, Rust does not.
Accessing `null` causes a program crash.

This means that all functions in which pointers can be passed must check for `null`.
The Rust Patience C binding contains a lot of such functions.

To start a new game, the `patience_create_game()` function returns a pointer to the game instance.

[source,rust,linenumbers]
.Create new game
----
#[no_mangle]
pub extern "C" fn patience_create_game() -> *mut Box<Game> {
    // returns a pointer:  *mut Box<Game>
}
----

This pointer is the first input parameter for all other game functions.

[source,rust,linenumbers]
.Move a card game function
----
#[no_mangle]
pub extern "C" fn patience_do_move(game_ref: *mut Box<Game>, 
                                   start: *const c_char,
                                   index: c_uint,
                                   target: *const c_char) -> *const c_char {

    // perform move implementation
}
----

`game_ref`, `start` and `target` are input pointer.
And the function returns a pointer.

The `game_ref` pointer is used to dereference the Patience instance.
However, this is only successful if the pointer is not `null`.
It is therefore necessary to check that `game_ref` is not `null` before dereferencing.
The primitive type {link-rust_pointer}[`pointer`] offers funtions to check wether the reference is `null` or not.

[source,rust,linenumbers]
.Move function with game pointer `null` check
----
#[no_mangle]
pub extern "C" fn patience_do_move(game_ref: *mut Box<Game>, 
                                   start: *const c_char,
                                   index: c_uint,
                                   target: *const c_char) -> *const c_char {

    if game_ref.is_null() {  <1>
        return PATIENCE_DO_MOVE_NO_GAME_REFERENCE
    }
    
    ...
}
----

<1> `game_ref` is a Rust primitive pointer type and must be checked for `null`.

All pointers in the function parameters must be checked for `null`, regardless of whether they are intentional, like `target`, or unintentional, like `start`.

=== Returning `Vec<T>`

One of the trickiest parts of the API was the query return for Discard Pile, Stackpiles and Columns.
These are defined in the Rust API by `Vec<CardSymbols>`.

The Rust API defines `Vec<CardSymbols>` as the query return for Discard Pile, Stackpiles, and Columns.
This was the most challenging aspect of the Rust C binding.

[source,rust,linenumbers]
.Rust `CardSymbols` definition
----
pub struct CardSymbols {
    pub card_symbol: char,
    pub suit_symbol: Option<char>,
    pub value: Option<u32>,
}
----

This API is not usable in C.
C `char` and Rust `char` have completely different semantics.
And C does not know anything about the `option` enumeration.

Therefor the first step is to define a more C compatible `CardSymbols`.

[source,rust,linenumbers]
.C compatible `CardSymbols` definition
----
#[repr(C)]
#[derive(Copy, Clone)]
pub struct PatienceCardSymbols {
    pub card_symbol: *const c_char,
    pub suit_symbol: *const c_char,
    pub value: c_unit
}
----


