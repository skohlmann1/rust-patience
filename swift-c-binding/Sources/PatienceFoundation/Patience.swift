// Rust Patience Game, C Binding for Swift
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import RustPatience

public class Patience {
    
    let game: UnsafeMutablePointer<UnsafeMutablePointer<Game>?>
    
    public init() throws {
        if let instance = RustPatience.patience_create_game() {
            self.game = instance
        } else {
            throw PatienceError.initalization
        }
    }
    
    public func talon() -> UInt {
        RustPatience.patience_query_talon_len(self.game)
    }

    public func isSolvable() -> Bool {
        RustPatience.patience_query_is_solvable(self.game)
    }

    public func isFinished() -> Bool {
        RustPatience.patience_query_is_finished(self.game)
    }

    public func doMove(_ cmd: MoveCommand) -> Result<Void,PatienceError> {

        let start = cmd.start.withCString { pathPtr in
            pathPtr
        }
        
        let result = Int32(RustPatience.patience_do_move(self.game, start, cmd.index ?? 0, cmd.target ?? nil))
        switch result {
        case RustPatience.PATIENCE_DO_MOVE_OK: return .success(());
        default: return .failure(.moveNotPossible)
        }
    }
    
    public func queryDiscardPile(_ query: DiscardPileQuery = DiscardPileQuery()) -> [CardSymbols] {
        let result = RustPatience.patience_query_discardpile(self.game, query.all)
        let retval = queryPileResultToCardySymbolsArray(arrayRef: result)

        RustPatience.patience_destroy_card_symbols_array(result)
        
        return retval
    }

    public func queryColumn(_ query: ColumnQuery) -> [CardSymbols] {
        let result = RustPatience.patience_query_column(self.game, query.columnIndex)
        let retval = queryPileResultToCardySymbolsArray(arrayRef: result)

        RustPatience.patience_destroy_card_symbols_array(result)
        
        return retval
    }

    public func queryStackPile(_ query: StackPileQuery) -> [CardSymbols] {
        
        let suit = query.suit.cString(using: .ascii)!.first

        let result = RustPatience.patience_query_stackpile(self.game, suit!)
        let retval = queryPileResultToCardySymbolsArray(arrayRef: result)

        RustPatience.patience_destroy_card_symbols_array(result)
        
        return retval
    }

    public func querySuggestions(_ query: SuggestionQuery) -> [MoveSuggestion] {
        
        let start = query.start.withCString { pathPtr in
            pathPtr
        }
        let result = RustPatience.patience_query_suggestions(self.game, start, query.index ?? 0)

        let array = result?.pointee?.pointee
        var retval: Array<MoveSuggestion> = Array.init()

        if let array {
            for i in 0..<array.array_len {
                let patienceMoveSuggestion = RustPatience.patience_move_suggestion_from_array(result, i)

                let ms = patienceMoveSuggestion!.pointee
                let start = String(cString: ms.start)
                let target = String(cString: ms.target)

                let index: UInt32? = if ms.index == 0 {
                    nil
                } else {
                    UInt32(ms.index)
                }
                
                let moveSuggestion =  MoveSuggestion(start: start, index: index, target: target)

                retval.append(moveSuggestion)
            }
        }

        RustPatience.patience_destroy_move_suggestion_array(result)
        
        return retval
    }

    
    public func columnIndices() -> [UInt8] {
        let result = RustPatience.patience_query_column_indices(self.game)
        let array = result?.pointee?.pointee
        var retval: Array<UInt8> = Array.init()

        if let array {
            for i in 0..<array.len {
                let index = RustPatience.patience_column_index_from_column_indices(result, i)
                if index != 0 {  // 0 represents unsupported index
                    retval.append(index)
                }
            }
        }
        
        RustPatience.patience_destroy_column_indices(result)
        
        return retval
    }
    
    deinit {
        RustPatience.patience_destroy_game(self.game)
    }
}

/// The MoveCommand must contain names of the different game piles and columns
/// where cards can be moved from and to.
///
/// Column names are starting with character `c` and a following number between 1 and 7 (inclusive).
/// E.g. `c1`, `c2` or `c7`.
///
/// Stackpiles are represented by the starting character of the suit they are representing.
/// E.g. `s` for spade or `c` for club.
///
/// The discard pile is represented by the `dp` combination.
/// In case `index` and `target` are `nil` a move from talon t discard pile will be performed.
///
/// It's also possible to define an index of a column card to move a slice of cards
/// from on column to another column. Such a numerical value must follow the column name.
/// E.g. `c1 11 c3` means all cards starting from index 11 till the top card will be moved
/// to column `c3` if and only if the rules allow moving.
///
/// The index is 1 based. This means the first index is not 0 but 1.
///
/// All letters are case-insenstive.
///
/// - start: the start pile (talon, stackpile, column, discard pile)
/// - index: an optional index for column to column moves
/// - target: the optional target pile. Can be a column or a stackpile
public struct MoveCommand {
    public let start: String;
    public let index: UInt32?;
    public let target: String?;
    
    public init(start: String, index: UInt32?, target: String?) {
        self.start = start
        self.index = index
        self.target = target
    }
}

public struct SuggestionQuery {
    public let start: String;
    public let index: UInt32?;
    
    public init(start: String, index: UInt32?) {
        self.start = start
        self.index = index
    }
}

public struct MoveSuggestion {
    public let start: String;
    public let index: UInt32?;
    public let target: String;
    
    public init(start: String, index: UInt32?, target: String) {
        self.start = start
        self.index = index
        self.target = target
    }
}


public enum PatienceError: Error {
    case initalization
    case moveNotPossible
    case unsupportedSuit
}

/// Contains information about the card symbols.
public struct CardSymbols : Identifiable, Hashable {
    /// Value will be from U+1F0A0 to U+1F0DE for all defined cards as of Unicode 15.1.0 but not U+1F0BF or U+1F0CF.
    ///
    /// See [Playing cards in Unicode](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode)
    /// for further information.
    public let symbol: String;

    /// Value will be ♠ (U+2660) or ♥ (U+2665) or ♦ (U+2666) or ♣ (U+2663).
    ///
    /// See [Playing cards in Unicode](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode)
    /// for further information.
    ///
    /// In case `suit` is `nil` it's an _unknown_ card for the player.
    public let suit: String?;

    /// Value will be between 1 and 13.
    ///
    /// - 1 for Ace
    /// - 11 for Jack
    /// - 12 for Queen
    /// - 13 for King
    ///
    /// A card where `value` is `nil` is an _unknown_ card for the player.
    public let value: UInt?;
    
    /// Indicates wether a card represented by the symbols is _known_  or _unknown_
    ///
    /// - Returns: `true` if and only if the card in _known_. `false` otherwise
    public func isKnown() -> Bool {
        if self.suit == nil {
            return false;
        }
        return true;
    }
    
    public init(symbol: String, suit: String?, value: UInt?){
        self.symbol = symbol
        self.suit = suit
        self.value = value
    }
    
    public var id: String {
        get {
            return "\(self.symbol)"
        }
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
}

/// Query data for the discard pile.
///
/// See also ``Patience/Patience/queryDiscardPile(_:)``
public struct DiscardPileQuery {
    /// Fetch all card symbols if and only if `all` is `true`. Top card otherwise.
    ///
    /// Default: `false`
    public let all: Bool;
    
    public init() {
        self.all = false
    }

    public init(all: Bool) {
        self.all = all
    }
}

/// Query data for the columns.
///
/// See also ``Patience/Patience/queryColumn(_:)``
public struct ColumnQuery {
    /// The column index (e.g. result from ``Patience/Patience/columnIndices()``
    public let columnIndex: UInt8;
    
    public init(columnIndex: UInt8){
        self.columnIndex = columnIndex
    }
}

/// Query data for the columns.
///
/// See also ``Patience/Patience/queryStackPile(_:)``
public struct StackPileQuery {

    public let suit: String
    
    /// - `suit` - Must be either be `c` for Club, `d` for Diamond, `h` for Heart or `s` for Spade. Otherwise a `PatienceError` will be thrown
    public init(suit: String) throws {
        if suit != "c" && suit != "d" && suit != "h" && suit != "s" {
            throw PatienceError.unsupportedSuit
        }
        self.suit = suit
    }
}

func queryPileResultToCardySymbolsArray(arrayRef: UnsafeMutablePointer<UnsafeMutablePointer<PatienceCardSymbolsArray>?>?) -> [CardSymbols] {
    let array = arrayRef?.pointee?.pointee
    var retval: Array<CardSymbols> = Array.init()

    if let array {
        for i in 0..<array.array_len {
            let card = RustPatience.patience_card_symbol_from_array(arrayRef, i)
            let cs = patienceCardSymbolsToCardSymbols(card)
            retval.append(cs)
        }
    }
    
    return retval
}

func patienceCardSymbolsToCardSymbols(_ patienceCard: UnsafeMutablePointer<PatienceCardSymbols>?) -> CardSymbols {
    
    let cardSymbol = patienceCard!.pointee
    let symbol = String(cString: cardSymbol.card_symbol)

    let suit: String? = if let ref = cardSymbol.suit_symbol {
        String(cString: ref)
    } else {
        nil
    }

    let value: UInt? = if cardSymbol.value == 0 {
        nil
    } else {
        UInt(cardSymbol.value)
    }
    
    return CardSymbols(symbol: symbol, suit: suit, value: value)
}
