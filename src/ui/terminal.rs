//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use patience_core::application::*;
use super::*;
use std::fmt::Write as _;
use std::io::{self, Write};
use colored::Colorize;
use colored::ColoredString;

pub fn play(game : &mut Game) {

    let mut solvable = false;
    let mut commands = 0;

    // Start the game loop
    loop {
        output_playing_field(game);

        if game.query_is_finished() {
            println!("{} moves, done.\nCongratulation.", commands);
            break
        }

        let player_input = get_player_input();
        if let Ok(move_command) = parse_command(&player_input) {
            if let Err(Some(desc)) = game.do_move(&move_command) {
                println!("{}", desc)
            } else {
                commands += 1;
                // Query only once if solvable
                if !solvable {
                    if game.query_is_solvable() {
                        println!("Patience is solvable");
                        solvable = true;
                    }
                }
            }
        } else {
            if player_input.trim().to_lowercase().starts_with('?') {
                print_help();
            } else if player_input.trim().to_lowercase().starts_with('q') {
                println!("Bye");
                break;
            } else {
                println!("Illegal move command: {}", player_input);
            }
        }
    }
}

fn print_help() {
    println!("Help\n====\n");
    println!("First two alphabetic characters of a line describe the start and target for an");
    println!("input command. These are:\n");
    println!("- 'c1' to 'c7' for the columns");
    println!("- 's', 'c', 'd' and 'h' for the stack pile suits");
    println!("- 'dp' for the discard pile\n");
    println!("Moves are possible from\n");
    println!("- talon (not visible) to discard pile.");
    println!("  Type 'dp' and hit enter to execute the command.\n");
    println!("- discard pile to columns or stack pile");
    println!("  Type 'dp' followed by 'c1' to 'c7' or 's', 'c', 'd' or 'h' and hit enter.");
    println!("  Example: 'dp c1' to move a card from discard pile to column 1.");
    println!("           If the move is possible, the card is moved. Otherwise");
    println!("           the card is still at it's position on the discard pile.\n");
    println!("- Type 'c1 c2' to move the top card from column 1 to column 2 if possible.");
    println!("- Type 'c1 2 c2' to move all card from column 1 index 2 and below to column 2.");
    println!("       Find the index value (1-19) below the last column (c7)");
    println!("- Type 's c2' to move a card from Spade stack pile to column 2.\n");
    println!("- Press 'q' for quit. It immediately ends the game.");
}

fn get_player_input() -> String {

    let stdout = io::stdout();
    let mut buf = io::BufWriter::new(stdout);
    let _ = write!(buf, "Your move please (? for help): ");
    let _ = buf.flush();

    let mut input = String::new();

    let _ = io::stdin().read_line(&mut input);

    input.trim().to_string()
}

fn output_playing_field(game : &Game) {
    println!("");
    for i in game.query_column_indices() {
        let query_column = QueryColumn{ column_index: i};
        print!("{}", "c".yellow());
        print!("{} ", i.to_string().yellow());
        for s in game.query_column(&query_column) {
            print!("{} ", colorized(&s));
        }
        println!("");
    }
    println!("{}", "    1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19\n".bright_blue());

    let suits = vec!['s', 'h', 'd', 'c'];
    for suit in suits {
        let query_stackpile = QueryStackPile{suit: suit};
        match game.query_stackpile(&query_stackpile) {
            Some(v) => {
                match v.last() {
                    Some(last) => println!("{}  {}", String::from(suit).yellow(), colorized(last)),
                    None => println!("{}  empty", String::from(suit).yellow())

                }
            },
            None => println!("{}  empty", String::from(suit).yellow())
        }
    }
    println!("");

    let query_discardpile = QueryDiscardPile{ all: false };
    print!("{} ", "dp".yellow());
    match game.query_discardpile(&query_discardpile).last() {
        Some(symbol) => print!("{}  ", colorized(symbol)),
        None => print!("empty")
    }
    println!("  T{}\n", game.query_talon_len());
}

fn colorized(symbol: &CardSymbols) -> ColoredString {
    let short = cardsymbols_to_string(symbol);
    if let Some(suit) = symbol.suit_symbol {
        match suit {
            '♠' | '♣' => short.bright_black(),
            '♥' | '♦' => short.red(),
            _ => ColoredString::from(short).strikethrough()
        }
    } else {
        ColoredString::from(short).strikethrough()
    }
}

fn cardsymbols_to_string(cardsymbols: &CardSymbols) -> String {
    let mut card = String::new();
    let _ = match cardsymbols.value {
        Some(v) => {
            match v {
                1 => write!(&mut card, "{}A ", cardsymbols.suit_symbol.unwrap()),
                10 => write!(&mut card, "{}10", cardsymbols.suit_symbol.unwrap()),
                11 => write!(&mut card, "{}J ", cardsymbols.suit_symbol.unwrap()),
                12 => write!(&mut card, "{}Q ", cardsymbols.suit_symbol.unwrap()),
                13 => write!(&mut card, "{}K ", cardsymbols.suit_symbol.unwrap()),
                _ => write!(&mut card, "{}{} ", cardsymbols.suit_symbol.unwrap(), v)

            }
        },
        None => write!(card, "XXX")
    };

    card
}
