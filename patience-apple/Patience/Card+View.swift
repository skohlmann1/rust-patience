// Rust Patience Game, Swift Frontend
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


import Foundation
import SwiftUI
import PatienceFoundation


struct Card {
    let ratio: Double
    let width: Double

    var height: Double { get {
            return self.width * self.ratio
        }
    }

    init(width: Double,
         ratio: Double = 1.25) {
        self.width = width
        self.ratio = ratio
    }
}

struct CardView : View {
    
    let index: Int
    let symbols: CardSymbols
    
    var body: some View {
        symbols.asView()
            .onHover { hovered in
#if os(macOS)
                DispatchQueue.main.async {
                    if hovered && self.symbols.isKnown() {
                        NSCursor.pointingHand.push()
                    } else {
                        NSCursor.pop()
                    }
                }
#endif
            }
    }
}


extension CardSymbols {
    
    func asView() -> some View {
        return cardView(card: self)
    }
    
    func cardName() -> String {
        cardSuitToAscii(suit: self.symbol)
    }
}

func cardView(card: CardSymbols) -> some View {
    
    if !card.isKnown() || card.suit == nil || card.value == nil {
        return loadAndConfigureImage("back5")
        
    }
    
    var cardName = cardName(card)
    
    // NOTE: as I'm from Germany I don't name cards "sa" or "hj", because of him.
    // So such short names are replaced with
    //
    // - "lemmy" for "sa".
    // - "zaphod" for "hj".
    if cardName == "sa" {
        cardName = "lemmy"
    }
    if cardName == "hj" {
        cardName = "zaphod"
    }
    
    return loadAndConfigureImage(cardName)
}

fileprivate func loadAndConfigureImage(_ name: String) -> some View {
    return Image(name).resizable().aspectRatio(contentMode: .fit)
}

fileprivate func cardName(_ card: CardSymbols) -> String {
    if !card.isKnown() || card.suit == nil || card.value == nil {
        return "unknown"
    }
    return "\(cardSuitToAscii(suit: card.suit!))\(cardValueToAscii(value: card.value!))"
}

func cardView(special: SpecialCard) -> some View {
    switch special {
    case .empty: return loadAndConfigureImage("empty")
    case .add: return loadAndConfigureImage("addcard")
    case .back: return loadAndConfigureImage("back5")
    }
}


enum SpecialCard {
    case empty
    case add
    case back
}

/// - 1 to `a`
/// - 11 to `j`
/// - 12 to `q`
/// - 13 to `k`
fileprivate func cardValueToAscii(value: UInt) -> String {
    switch value {
    case 1: return "a"
    case 11: return "j"
    case 12: return "q"
    case 13: return "k"
    default: return "\(value)"
    }
}

/// - ♠ to `s`
/// - ♥ to `h`
/// - ♦ to `d`
/// - ♣ to `c`
func cardSuitToAscii(suit: String) -> String {
    switch suit {
    case "♠": return "s"
    case "♥": return "h"
    case "♦": return "d"
    case "♣": return "c"
    default: return ""
    }
}

#Preview {
    VStack {
        CardView(index: 1, symbols: CardSymbols(symbol: "d", suit: nil, value: nil))
        CardView(index: 2, symbols: CardSymbols(symbol: "d", suit: "♠", value: 1))
    }
}
