//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::slice::Iter;
use std::vec::*;
use std::fmt::{self};

use crate::domain::*;
use crate::domain::card::*;

use self::patience::*;

pub struct Game {
    patience: Patience
}

impl Game {
    /// Creates a new Game with a shuffeled card deck.
    pub fn new() -> Game {
        Game {
            patience: Patience::new()
        }
    }

    fn from(patience: Patience) -> Game {
        Game {
            patience
        }
    }
}

// Command part of the application
impl Game {

    pub fn do_move(&mut self, cmd : &MoveCommand) -> Result<(), Option<&str>> {
        match cmd.start.to_lowercase().as_str() {
            "dp" => {
                match cmd.target {
                    None => {
                        self.patience.from_talon_to_discardpile();
                        Ok(())
                    },
                    Some(target) => match target {
                        "c" | "s" | "h" | "d" | "♠" => {
                            self.patience.from_discardpile_to_stackpile()
                        },
                        "c1" | "c2" | "c3" | "c4" | "c5" | "c6" | "c7" => {
                            let column_index = self.column_name_to_index(target);
                            self.patience.from_discardpile_to_column(column_index)
                        },
                        _ => Err(None)
                    }
                }
            },
            "c" | "s" | "h" | "d" => {
                let suit: Suit = Suit::of(cmd.start.to_lowercase().as_str()).unwrap();
                match cmd.target {
                    Some(column) => {
                        let column_number = self.column_name_to_index(&column);
                        self.patience.from_stackpile_to_column(&suit, column_number)
                    },
                    None => Err(Some("No target for stackpile move"))
                }
            },
            "c1" | "c2" | "c3" | "c4" | "c5" | "c6" | "c7" => {
                match cmd.target {
                    Some(target) => match target {
                         "c" | "s" | "h" | "d" => {
                            let column_number = self.column_name_to_index(&cmd.start);
                            self.patience.from_column_to_stackpile(column_number)
                         },
                         "c1" | "c2" | "c3" | "c4" | "c5" | "c6" | "c7" => {
                            let from_column_number = self.column_name_to_index(&cmd.start);
                            let to_column_number = self.column_name_to_index(target);
                            let card_index = match cmd.index {
                                Some(index) => Some((index -1) as u8),
                                _ => None
                            };
                            let column_command = ColumnToColumnCommand {
                                from_column_number,
                                to_column_number,
                                card_index
                            };
                            self.patience.from_column_to_column(&column_command)
                         },
                         _ => Err(Some("No supported target"))
                    },
                    None => Err(Some("No target for stackpile move"))
                }
            },
            _ => Err(Some("Unsupported command"))
        }
    }

    fn column_name_to_index(&self, column_name: &str) -> u8 {
        let colname = column_name.to_lowercase();
        match colname.as_str() {
            "c1" => 1,
            "c2" => 2,
            "c3" => 3,
            "c4" => 4,
            "c5" => 5,
            "c6" => 6,
            "c7" => 7,
            _ => u8::MAX
        }
    }
}

// Query part of the application
impl Game {

    /// The result contains the cards of the discard pile where the card at position zero
    /// represents the bottom card of the pile and the [last](std::vec::Vec#last) position
    /// contains the top visible card of the pile.
    /// 
    /// Discard pile cards are always [_known_](CardSymbols#value).
    pub fn query_discardpile(&self, cmd: &QueryDiscardPile) -> Vec<CardSymbols> {
        let mut retval = Self::iter_to(self.patience.cards_from_discard_pile());
        if cmd.all {
            return retval;
        }

        match retval.pop() {
            Some(card_symbol) => vec![card_symbol],
            None => vec![]
        }
    }

    pub fn query_column_indices(&self) -> Vec<u8> {
        self.patience.column_indices().collect()
    }

    /// The result contains the cards of the column where the card at position zero
    /// represents the bottom card of the column and the [last](std::vec::Vec#last)
    /// position contains the top visible card of the pile.
    /// 
    /// The last card is always a _known_ card where the card at position zero is
    /// either _known_ or _unknown_.
    /// 
    /// See [CardSymbols] for further information about _known_ and _unknown_ cards.
    pub fn query_column(&self, query: &QueryColumn) -> Vec<CardSymbols> {
        match self.patience.cards_from_column(query.column_index) {
            None => vec![],
            Some(iter) => Self::iter_to(iter)
        }
    }

    fn iter_to(iter: Iter<Card>) -> Vec<CardSymbols> {
        iter.map(|card| CardSymbols::of(card)).collect()
    }

    /// The result contains the cards of the stack pile where the card at position zero
    /// represents the bottom card of the pile and the [last](std::vec::Vec#last) position
    /// contains the top visible card of the pile.
    /// 
    /// The card at position zero, if and only if available, is always an 
    /// [Ace](CardSymbols#value).
    pub fn query_stackpile(&self, query: &QueryStackPile) -> Option<Vec<CardSymbols>> {
        let suit = String::from(query.suit);
        match Suit::of(&suit) {
            Ok(suit) => Some(Self::iter_to(self.patience.cards_from_stackpile(&suit))),
            _ => None,
        }
    }

    pub fn query_talon_len(&self) -> usize {
        self.patience.talon_len()
    }

    pub fn query_is_finished(&self) -> bool {
        self.patience.is_finished()
    }

    pub fn query_is_solvable(&self) -> bool {
        self.patience.is_solvable()
    }

    /// Try to find targets for the card in the game based on the position of the card.
    pub fn query_suggestions(&self, query: &QuerySuggestion) -> Vec<MoveSuggestion> {
        // find template card
        let template = self.find_template_card(query);

        let suggestions = match template.card {
            None => Suggestions::empty(),
            Some(card) => {
                self.patience.suggest_targets(&card, template.sliced)
            }
        };

        let mut retval = Vec::new();
        for suit in &suggestions.suits {
            let string = String::from(self.suit_to_ascii(suit));
            retval.push(MoveSuggestion{start: query.start.to_string(), index: query.index.clone(), target: string});
        }
        for column_index in &suggestions.column_indices {
            let string: String = format!("c{}", column_index);
            retval.push(MoveSuggestion{start: query.start.to_string(), index: query.index.clone(), target: string});
        }
        retval
    }

    fn suit_to_ascii(&self, suit: &Suit) -> &str {
        match suit {
            &Suit::Club => "c",
            &Suit::Spade => "s",
            &Suit::Diamond => "d",
            &Suit::Heart => "h",
        }
    }

    fn find_template_card(&self, query: &QuerySuggestion) -> SuggestionTemplate {
        match query.start.to_lowercase().trim() {
            "dp" => {
                SuggestionTemplate { card: self.patience.cards_from_discard_pile().last(), sliced: false }
            },
            "c" | "s" | "h" | "d" => {
                let suit: Suit = Suit::of(query.start.to_lowercase().as_str()).unwrap();
                SuggestionTemplate { card: self.patience.cards_from_stackpile(&suit).last(), sliced: false }
            },
            "c1" | "c2" | "c3" | "c4" | "c5" | "c6" | "c7" => {
                let column: u8 = self.column_name_to_index(&query.start);
                match self.patience.cards_from_column(column) {
                    None => SuggestionTemplate { card: None, sliced: false },
                    Some(iter) => {
                        match query.index {
                            None => SuggestionTemplate { card: iter.last(), sliced: false },
                            Some(index) => {
                                let cards: Vec<&Card> = iter.collect();
                                let u_index = (index - 1) as usize;
                                match cards.get(u_index) {
                                    None => SuggestionTemplate { card: None, sliced: false },
                                    Some(value) => {
                                        let sliced = if u_index < cards.len() { true } else { false };
                                        SuggestionTemplate { card: Some(*value), sliced }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            _ => SuggestionTemplate { card: None, sliced: false }
        }
    }
}

struct SuggestionTemplate<'a> {
    card: Option<&'a Card>,
    sliced: bool
}

/// Contains [`MoveCommand`] compatible values.
#[derive(Eq, Hash, PartialEq, Debug, Clone)]
pub struct MoveSuggestion {
    pub start: String,
    pub index: Option<u32>,
    pub target: String
}


#[derive(Debug)]
pub struct MoveCommand<'a> {
    pub start: &'a str,
    pub index: Option<u32>,
    pub target: Option<&'a str>
}

impl <'a> MoveCommand<'a> {

    pub fn builder(start: &str) -> MoveCommand {
        MoveCommand {
            start,
            target: None,
            index: None
        }
    }

    // Builder pattern
    pub fn with_target(&mut self, target: &'a str) -> &mut Self {
        self.target = Some(target);
        self
    }

    pub fn with_index(&mut self, index: u32) -> &mut Self {
        self.index = Some(index);
        self
    }
    pub fn with_start(&mut self, start: &'a str) -> &mut Self {
        self.start = start;
        self
    }

}

/// The MoveCommand must contain names of the different game piles and columns
/// where cards can be moved from and to.
///
/// Column names are starting with character `c` and a following number between 1 and 7 (inclusive).
/// E.g. `c1`, `c2` or `c7`.
///
/// Stackpiles are represented by the starting character of the suit they are representing.
/// E.g. `s` for spade or `c` for club. 
///
/// The discard pile is represented by the `dp` combination.
/// In case `index` and `target` are `nil` a move from talon t discard pile will be performed.
///
/// It's also possible to define an index of a column card to move a slice of cards
/// from on column to another column. Such a numerical value must follow the column name.
/// E.g. `c1 11 c3` means all cards starting from index 11 till the top card will be moved
/// to column `c3` if and only if the rules allow moving.
/// 
/// The index is 1 based. This means the first index is not 0 but 1.
///
/// All letters are case-insenstive.
/// 
/// See also [`MoveSuggestion`].
impl <'a> MoveCommand<'a> {
    pub fn is_empty(&self) -> bool {
        self.start == ""
    }
}

impl <'a> fmt::Display for MoveCommand<'a>  {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.start)?;
        if let Some(index) = &self.index {
            write!(f, " {}", index)?;
        }
        if let Some(target) = &self.target {
            write!(f, " {}", target)?;
        }
        write!(f, "")
    }
}


pub struct QueryColumn {
    pub column_index: u8
}

pub struct QueryStackPile {
    pub suit : char,
}

pub struct QueryDiscardPile {
    pub all : bool
}

#[derive(Debug)]
pub struct QuerySuggestion<'a> {
    /// The start field. See `doMove` for value description.
    pub start: &'a str,
    /// An optional index within a column. In case `start` is not a column the value must be ignored.
    pub index: Option<u32>,
}

impl <'a> QuerySuggestion<'a> {

    pub fn builder(start: &str) -> QuerySuggestion {
        QuerySuggestion {
            start,
            index: None
        }
    }

    // Builder pattern
    pub fn with_index(&mut self, index: u32) -> &mut Self {
        self.index = Some(index);
        self
    }
}

impl <'a> fmt::Display for QuerySuggestion<'a>  {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.start)?;
        if let Some(index) = &self.index {
            write!(f, " {}", index)?;
        }
        write!(f, "")
    }
}


pub struct CardSymbols {
    /// Value will be from U+1F0A0 to U+1F0DE for all defined playing cards of Unicode 15.1.0 but not U+1F0BF or U+1F0CF.
    /// 
    /// See [Playing cards in Unicode](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode)
    /// for further information.
    pub card_symbol: char,

    /// Value will be ♠ (U+2660) or ♥ (U+2665) or ♦ (U+2666) or ♣ (U+2663).
    /// 
    /// See [Playing cards in Unicode](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode)
    /// for further information.
    /// 
    /// A card where `suit_symbol` is `None` is an _unknown_ card for the player.
    pub suit_symbol: Option<char>,

    /// Value will be between 1 and 13.
    ///
    /// 1 for Ace<br />
    /// 11 for Jack<br />
    /// 12 for Queen<br />
    /// 13 for King
    ///
    /// A card where `value` is `None` is an _unknown_ card for the player.
    pub value: Option<u32>,
}

impl CardSymbols {
    fn of(card: &Card) -> CardSymbols {
        if card.is_unknown() {
            CardSymbols {
                card_symbol: card.symbol(),
                suit_symbol: None,
                value: None
            }
        } else {
            CardSymbols {
                card_symbol: card.symbol(),
                suit_symbol: Some(card.suit.symbol()),
                value: Some(card.value)
            }
        }
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use self::patience::tests::{empty, fill_in_column, fill_in_discard_pile, fill_in_stackpile};


    #[test]
    fn simple_creation() {
        // Given
        let game = Game::new();

        // Then
        assert_eq!(game.query_is_finished(), false);
        assert_eq!(game.query_talon_len(), 24);
    }

    #[test]
    fn simple_move_command() {
        // Given
        let mut game = Game::new();
        let discard_pile_query = QueryDiscardPile { all: false };

        // Then
        assert!(game.query_discardpile(&discard_pile_query).is_empty());

        // And when
        let move_cmd = MoveCommand::builder("dp");
        let _ = game.do_move(&move_cmd);

        // Then
        assert_eq!(game.query_discardpile(&discard_pile_query).len(), 1);
        assert_eq!(game.query_talon_len(), 23);
    }

    #[test]
    fn all_column_indices() {
        // Given
        let game = Game::new();

        // Then
        assert_eq!(game.query_column_indices().len(), 7);
    }

    #[test]
    fn unknown_card() {
        // Given
        let game = Game::new();
        let query_column = QueryColumn { 
            column_index: *game.query_column_indices().last().unwrap()
        };
        let column = game.query_column(&query_column);

        // When
        let first = column.first().unwrap();

        // Then
        assert_eq!(first.card_symbol, char::from_u32(0x1F0A0).unwrap());
        assert_eq!(first.suit_symbol, None);
        assert_eq!(first.value, None);
    }

    #[test]
    fn query_discard_pile_not_all() {
        // Given
        let mut game = Game::new();
        let discard_pile_query = QueryDiscardPile { all: false };

        // When
        let move_cmd = MoveCommand::builder("dp");
        let _ = game.do_move(&move_cmd);
        let _ = game.do_move(&move_cmd);

        // Then
        assert_eq!(game.query_discardpile(&discard_pile_query).len(), 1);
        assert_ne!(game.query_discardpile(&discard_pile_query).first().unwrap().suit_symbol, None);
    }

    #[test]
    fn query_discard_pile_all() {
        // Given
        let mut game = Game::new();
        let discard_pile_query = QueryDiscardPile { all: true };

        // When
        let move_cmd = MoveCommand::builder("dp");
        let _ = game.do_move(&move_cmd);
        let _ = game.do_move(&move_cmd);

        // Then
        assert_eq!(game.query_discardpile(&discard_pile_query).len(), 2);

        assert_ne!(game.query_discardpile(&discard_pile_query).first().unwrap().suit_symbol, None);
        assert_ne!(game.query_discardpile(&discard_pile_query).first().unwrap().value, None);
        assert_ne!(game.query_discardpile(&discard_pile_query).first().unwrap().card_symbol, char::from_u32(0x1F0A0).unwrap());

        assert_ne!(game.query_discardpile(&discard_pile_query).last().unwrap().suit_symbol, None);
        assert_ne!(game.query_discardpile(&discard_pile_query).last().unwrap().value, None);
        assert_ne!(game.query_discardpile(&discard_pile_query).last().unwrap().card_symbol, char::from_u32(0x1F0A0).unwrap());
    }

    #[test]
    fn query_suggestions_from_discard_pile() {
        // Given
        let mut patience = empty();

        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Heart);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1]);
        let suit_spade_card1: Card = Card::create_known(ACE, Suit::Spade);
        fill_in_stackpile(&mut patience, &Suit::Spade, vec![suit_spade_card1]);

        let discard: Card = Card::create_unknown(2, Suit::Club);
        fill_in_discard_pile(&mut patience, vec![discard]);

        let game = Game::from(patience);

        // When
        let suggestions = game.query_suggestions(&QuerySuggestion::builder("dp").with_index(2));

        // Then
        assert_eq!(suggestions.len(), 3);
        assert!(suggestions.contains(&MoveSuggestion {start: "dp".to_string(), index: Some(2), target: "c1".to_string()} ));
        assert!(suggestions.contains(&MoveSuggestion {start: "dp".to_string(), index: Some(2), target: "c2".to_string()} ));
        assert!(suggestions.contains(&MoveSuggestion {start: "dp".to_string(), index: Some(2), target: "c".to_string()} ));
 
    }

    #[test]
    fn query_suggestions_from_suit() {
        // Given
        let mut patience = empty();

        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Club);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        let suit_club_card2: Card = Card::create_known(2, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1, suit_club_card2]);

        let game = Game::from(patience);

        // When
        let suggestions = game.query_suggestions(&QuerySuggestion::builder("c"));

        // Then
        assert_eq!(suggestions.len(), 1);
        assert!(suggestions.contains(&MoveSuggestion {start: "c".to_string(), index: None, target: "c1".to_string()} ));
 
    }

    #[test]
    fn query_suggestions_from_colunn() {
        // Given
        let mut patience = empty();

        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Heart);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let col3_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col3_card2: Card = Card::create_known(2, Suit::Club);
        fill_in_column(&mut patience, 3, vec![col3_card1, col3_card2]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1]);

        let game = Game::from(patience);

        // When
        let suggestions = game.query_suggestions(&QuerySuggestion::builder("c3"));

        // Then
        assert_eq!(suggestions.len(), 3);
        assert!(suggestions.contains(&MoveSuggestion {start: "c3".to_string(), index: None, target: "c1".to_string()} ));
        assert!(suggestions.contains(&MoveSuggestion {start: "c3".to_string(), index: None, target: "c2".to_string()} ));
        assert_eq!(suggestions[0].target, String::from("c"));

    }


    #[test]
    fn query_suggestions_from_colunn_with_matching_index() {
        // Given
        let mut patience = empty();

        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Heart);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let col3_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col3_card2: Card = Card::create_known(2, Suit::Club);
        let col3_card3: Card = Card::create_known(ACE, Suit::Diamond);
        fill_in_column(&mut patience, 3, vec![col3_card1, col3_card2, col3_card3]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1]);

        let game = Game::from(patience);

        // When
        let suggestions = game.query_suggestions(&QuerySuggestion::builder("c3").with_index(2));

        // Then
        assert_eq!(suggestions.len(), 2);
        assert!(suggestions.contains(&MoveSuggestion {start: "c3".to_string(), index: Some(2), target: "c1".to_string()} ));
        assert!(suggestions.contains(&MoveSuggestion {start: "c3".to_string(), index: Some(2), target: "c2".to_string()} ));

    }

}
