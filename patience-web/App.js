/*
 *  Rust Patience Game, Web edition
 *  Copyright (C) 2024 Sascha Kohlmann
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import init, { Game, CardSymbols } from './pkg/wasm_patience_core_binding.js';
import CardStack, { Card } from './components/cardstack/cardstack.mjs';


await init();

export default class Patience extends HTMLElement {

    #pointerEventSupported = ("PointerEvent" in window);
    #touchEventSupported = ("TouchEvent" in window) && !this.#pointerEventSupported;

    #game;
    #initDone = false;

    #stackNameToStackId = new Map();
    #stackIdToStackName = new Map();

    state;

    constructor() {
        super();
        this.#game = Game.new();
    }

    connectedCallback() {
        if (!this.#initDone) {
            this.#mapIdsToNames();
            this.#initObserver();
            this.#updateColumns();
            this.suiteIds.forEach(id => this.#updateStack(id, []));
            this.#connectTalon();
            this.#initEvents();
            this.#initDone = true;
        }
    }

    #mapIdsToNames() {
        // Note: this is a predefinition what suit stack contains what suit.
        //       This is yet not optimal and must be fixed later.
        const suitIds = this.suiteIds;
        suitIds.forEach((id, index) => { this.#stackIdToStackName.set(id, Patience.suitNames.at(index)); });
        suitIds.forEach((id, index) => { this.#stackNameToStackId.set(Patience.suitNames.at(index), id); });

        const columnIds = this.columnIds;
        columnIds.forEach((id, index) => { index += 1; this.#stackIdToStackName.set(id, "c" + index) });
        columnIds.forEach((id, index) => { index += 1; this.#stackNameToStackId.set("c" + index, id) });

        const discardPileId = this.discardPileId;
        this.#stackIdToStackName.set(discardPileId, this.#discardPileName);
        this.#stackNameToStackId.set(this.#discardPileName, discardPileId);

        const talonId = this.talonId;
        this.#stackIdToStackName.set(talonId, "talon");
        this.#stackNameToStackId.set("talon", talonId.trim());

        /*
        console.debug(`stackIdToStackName: ${this.#stackIdToStackName.forEach((v, k) => console.log("id2name: " + k + ", " + v))}`);
        console.debug(`stackNameToStackId: ${this.#stackNameToStackId.forEach((v, k) => console.log("name2id: " + k + ", " + v))}`);
        */
    }

    #eventHandler(e) {
        switch (e.type) {
            case "talonEvent":
                this.#game.do_move(this.#discardPileName, undefined, "");
                this.#updatePatience();
                break;
        }
    }

    #connectTalon() {
        const talonId = this.talonId;
        if (talonId !== null) {
            const stack = this.#stackById(talonId);
            stack.addEventListener("click", e => {
                const talonEvent = new CustomEvent("talonEvent", { detail: { stackId: talonId } });
                this.#eventHandler(talonEvent);
            });
        }
    }

    #initObserver() {
        new MutationObserver(mutationsList => {
            mutationsList.forEach(entry => {
                if (entry.type === 'childList') {
                    entry.addedNodes.forEach(node => {
                        if (node.hasAttribute("card-type")) {
                            // if (this.#isInDropableCardStack(node)) {
                            //     this.#makeDropable(node);
                            // }
                            if (node.getAttribute("card-type") === "known") {
                                this.#makeDragable(node);
                            }
                        }
                    });
                }
            });
        }).observe(this, { childList: true, subtree: true });
    }

    /**
     * Checks wether a node is in a droppable card stack or not.
     * @param {*} node 
     * @returns bool
     */
    #isInDropableCardStack(node) {
        while (node) {
            if (node.id !== undefined && node.id !== null && (this.suiteIds.includes(node.id) || this.columnIds.includes(node.id))) {
                return true;
            }
            node = node.parentElement;
        }
        return false;
    }

    #parentWithStackId(node) {
        while (node) {
            if (node.id !== undefined && node.id !== null && this.#stackIdToStackName.has(node.id)) {
                return node.id;
            }
            node = node.parentElement;
        }
        return null;
    }

    #makeDropable(node) {
        const cls = this.dropableClass;
        if (!node.classList.contains(cls)) {
            node.classList.add(cls);
        }
    }

    #makeDragable(node) {
        const cls = this.dragableClass;
        if (!node.classList.contains(cls)) {
            node.classList.add(cls);
        }
    }

    #cardSymbolsToCard(cardSymbols) {
        if (cardSymbols.constructor !== Array) {
            return [this.#cardSymbolToCard(cardSymbols)];
        }
        const cards = [];
        cardSymbols.forEach(cardsymbol => {
            cards.push(this.#cardSymbolToCard(cardsymbol));
        });
        return cards;
    }

    #cardSymbolToCard(cardSymbol) {
        return new Card(cardSymbol.value, cardSymbol.suit_symbol);
    }

    get #allDropableCardStackElements() {
        // This code is yet heavy bound to the situations where the dropbale is
        // contains also a Stack ID. But maybe the Stack ID is part of a child
        // or a parent node. Might be fixed later.
        const elements = document.querySelectorAll("." + this.dropableClass);
        const retval = [];
        elements.forEach(e => {
            if (this.#stackIdToStackName.has(e.id)) {
                retval.push(e);
            }
        });
        return retval;
    }

    get columnIds() {
        const attribute = this.getAttribute('column-ids');
        if (attribute === undefined || attribute === null || attribute.trim() === "") {
            console.error(`No columns IDs available.`);
            return [];
        }
        const ids = attribute.split(" ");
        if (this.#game.query_column_indices().length != ids.length) {
            console.error(`Game requires ${this.#game.query_column_indices().length} columns but only ${ids.length} columns are available.`);
        }

        return ids;
    }

    get suiteIds() {
        const suitIds = this.getAttribute("suit-ids");
        if (suitIds === undefined || suitIds === null && suitIds.trim() === "") {
            console.error(`No suit IDs available.`);
            return []
        }
        return suitIds.split(" ");
    }

    get discardPileId() {
        const discardId = this.getAttribute("discard-pile-id");
        if (discardId === undefined || discardId === null && discardId.trim() === "") {
            console.error(`No discard pile ID available.`);
            return null;
        }
        return discardId;
    }

    get talonId() {
        const talonId = this.getAttribute("talon-id");
        if (talonId === undefined || talonId === null && talonId.trim() === "") {
            console.error(`No talon ID available.`);
            return null;
        }
        return talonId;
    }

    get dragableClass() {
        const cls = this.getAttribute("draggable-class");
        if (cls === null || cls.trim() === "") {
            console.warn("No draggable-class attribute found");
        }
        return cls;
    }

    get dropableClass() {
        const cls = this.getAttribute("droppable-class");
        if (cls === null || cls.trim() === "") {
            console.warn("No droppable-class attribute found");
        }
        return cls;
    }

    get overDropableClass() {
        const cls = this.getAttribute("over-droppable-class");
        if (cls === null || cls.trim() === "") {
            console.warn("No over-droppable-class attribute found");
        }
        return cls;
    }

    get draggingClass() {
        const cls = this.getAttribute("dragging-class");
        if (cls === null || cls.trim() === "") {
            console.warn("No dragging-class attribute found");
        }
        return cls;
    }

    static get suitNames() {
        return ["c", "d", "h", "s"];
    }

    get #clickIntervalMillis() {
        return 300;
    }
    get #discardPileName() {
        return "dp";
    }

    #initEvents() {
        // Note all event handlers
        if (this.#pointerEventSupported) {
            document.body.addEventListener("pointerdown", e => this.#handleDown(this, e), false);
            document.body.addEventListener("pointermove", e => this.#handleMove(this, e), false);
            document.body.addEventListener("pointerup", e => this.#handleUp(this, e), false);
        } else if (this.#touchEventSupported) {
            document.body.addEventListener("touchstart", e => this.#handleDown(this, e), false);
            document.body.addEventListener("touchmove", this.#handleMove(this, e), false);
            document.body.addEventListener("touchend", this.#handleUp(this, e), false);
        } else {
            document.body.addEventListener("mousedown", e => this.#handleDown(this, e), false);
            document.body.addEventListener("mousemove", e => this.#handleMove(this, e), false);
            document.body.addEventListener("mouseup", e => this.#handleUp(this, e), false);
        }
    }

    #isDragable(node) {
        return this.#draggable(node) !== null;
    }

    #draggable(node) {
        const dragCls = this.dragableClass;
        if (node && "closest" in node) {
            return node.closest("." + dragCls);
        }

        while (node) {
            if (node.classList !== undefined && node.classList.contains(dragCls)) {
                return node;
            }
            node = node.parentElement;
        }
        return null;
    }

    #isDropable(node) {
        const dropCls = this.dropableClass;
        if (node && "closest" in node) {
            return node.closest("." + dropCls);
        }

        while (node) {
            if (node.classList !== undefined && node.classList.contains(dropCls)) {
                return true;
            }
            node = node.parentElement;
        }
        return false;
    }

    #overDropable(position) {
        let retval = null;
        this.#allDropableCardStackElements.forEach(droppable => {
            const bounds = droppable.getBoundingClientRect();
            if (position.x >= bounds.left && position.x <= bounds.right && position.y >= bounds.top && position.y <= bounds.bottom) {
                retval = droppable;
            }
        });

        return retval;
    }

    #updateSuits() {
        Patience.suitNames.forEach(suitName => {
            const cards = this.#game.query_stackpile(suitName);
            const suitId = this.#stackNameToStackId.get(suitName);
            this.#updateStack(suitId, cards);
        });
    }

    #updateDiscardPile() {
        const cards = this.#game.query_discardpile(false);
        this.#updateStack(this.discardPileId, cards);
    }

    #updateColumns() {
        const ids = this.columnIds;
        const gameColumnsIndices = this.#game.query_column_indices();

        for (var i = 0; i < gameColumnsIndices.length; i += 1) {
            const cards = this.#game.query_column(gameColumnsIndices[i]);
            this.#updateStack(ids[i], cards);
        }
    }
    #updateStack(stackId, cardSymbols) {
        const stack = this.#stackById(stackId);
        if (stack === undefined || stack === null) {
            console.error(`No stack for ID ${stackId} available.`);
            return;
        }
        stack.updateCards(this.#cardSymbolsToCard(cardSymbols));
    }

    #updatePatience() {
        this.#updateColumns();
        this.#updateSuits();
        this.#updateDiscardPile();

        this.#fireUpdateEvent();
    }
    
    #fireUpdateEvent() {
        const mue = new MessageUpdateEvent();
        mue.talonSize = this.#game.query_talon_len();
        if (this.#game.query_is_solvable()) {
            mue.message = "solvable";
        }
        if (this.#game.query_is_finished()) {
            mue.message = "solved";
        }

        this.dispatchEvent(mue);
    }

    #stackById(stackId) {
        return document.getElementById(stackId);
    }

    #performMove(sourceId, index, targetId) {
        try {
            // console.debug(`performMove: args - sourceId: ${sourceId} - index: ${index} - targetId: ${targetId}`);
            const sourceStackName = this.#stackIdToStackName.get(sourceId);
            if (targetId === undefined && sourceStackName != this.#discardPileName) {
                return;
            }
            let targetStackName = this.#stackIdToStackName.get(targetId);
            if (targetStackName === null) {
                targetStackName = "d"; // diamond
            }
            if (index === null) {
                index = undefined;
            } else {
                index = parseInt(index) + 1;
            }
            const result = this.#game.do_move(sourceStackName, index, targetStackName);
        } catch (e) {
            console.error(`performMove: ${e}`);
        }
    }

    #performSuggestion(sourceId, index) {
        try {
            const sourceStackName = this.#stackIdToStackName.get(sourceId);
            if (index === null) {
                index = undefined;
            } else {
                index = parseInt(index) + 1;
            }
            const result = this.#game.query_suggestions(sourceStackName, index);
            if (result.length !== 0) {
                let first = result.at(0);
                var index = null;
                if (first.index === undefined) {
                    index = null;
                } else {
                    index = first.index;
                }
                const d = this.#game.do_move(first.start, index, first.target);
            }
         } catch (e) {
            console.error(`performSuggestion: ${sourceId}, ${index} - ${e}`);
        }
    }

    #handleDown(source, e) {
        if (e.target.hasAttribute("card-index") && source.#isDragable(e.target)) {
            document.body.style.touchAction = "none";
            const draggable = source.#draggable(e.target);
            e.preventDefault();
            const stackId = source.#parentWithStackId(draggable);
            let index = draggable.getAttribute("card-index");
            if (draggable.nextSibling === null) {
                index = null
            }

            source.state = new State();
            source.state.sourceStackId = stackId;
            source.state.index = index;
            source.state.overDropable = source.#isInDropableCardStack(draggable);
            source.state.eventPos = source.#pointerPosition(e);
            source.state.startPos = new Position(draggable.offsetLeft, draggable.offsetTop);
            source.state.startAt = e.timeStamp;

            const width = draggable.offsetWidth;
            const height = draggable.offsetHeight;

            const offsetParent = draggable.offsetParent;
            if (offsetParent !== null) {
                const cardContainer = new CardStack();
                cardContainer.classList.add(source.draggingClass);
                cardContainer.style.position = "absolute";
                cardContainer.style.left = draggable.offsetLeft + "px";
                cardContainer.style.top = draggable.offsetTop + "px";

                // handle siblings of downed draggable
                const cardElements = [draggable];
                let sibling = draggable.nextSibling
                while (sibling !== null) {
                    cardElements.push(sibling);
                    sibling = sibling.nextSibling;
                }

                const parent = draggable.parentElement;
                cardElements.forEach(card => {
                    parent.removeChild(card);
                    cardContainer.appendChild(card);
                });
                offsetParent.appendChild(cardContainer);

                cardContainer.style.width = width;
                cardContainer.style.height = height;

                source.state.element = cardContainer;
            }
        }
    }

    #handleMove(source, e) {
        if (source.state !== undefined && source.state !== null) {
            e.preventDefault();

            const eventPosition = source.#pointerPosition(e);
            source.state.element.style.left = (source.state.startPos.x + eventPosition.x - source.state.eventPos.x) + "px";
            source.state.element.style.top = (source.state.startPos.y + eventPosition.y - source.state.eventPos.y) + "px";

            // Check the droppable target by positon because otherwise
            // not possible as the event source is a wrong element.
            const overDropable = source.#overDropable(eventPosition);
            source.#removeAllOverDropables(); // For too fast movements
            if (overDropable !== null) {
                const stackId = source.#parentWithStackId(overDropable);
                if (stackId !== null) {
                    source.state.overDropable = true;
                    source.state.targetStackId = stackId;
                    if (overDropable.classList !== undefined) {
                        overDropable.classList.add(source.overDropableClass);
                    }
                } else {
                    source.state.overDropable = false;
                }
            } else {
                source.state.overDropable = false;
                source.state.targetStackId = null;
            }
        }
    }

    #handleUp(source, e) {
        source.#removeAllOverDropables(); // For too fast movements

        if (source.state !== undefined && source.state !== null && (e.timeStamp - source.state.startAt) < this.#clickIntervalMillis) {
            source.#performSuggestion(source.state.sourceStackId, source.state.index);
        } else if (source.state !== undefined && source.state !== null && source.state.overDropable) {
            e.preventDefault();
            document.body.style.touchAction = "auto";
            source.#performMove(source.state.sourceStackId, source.state.index, source.state.targetStackId);
        }
        if (source.state !== null && source.state !== undefined && source.state.element !== undefined && source.state.element !== null) {
            source.state.element.parentElement.removeChild(source.state.element);
        }
        source.#updatePatience();
        source.state = null;
    }

    #removeAllOverDropables() {
        const overDropableClass = this.overDropableClass
        document.querySelectorAll(`.${overDropableClass}`).forEach(overDropable => {
            if (overDropable.classList !== undefined) {
                overDropable.classList.remove(overDropableClass);
            }
        });
    }

    #pointerPosition(e) {
        let x = 0, y = 0;
        if (this.#touchEventSupported && e.targetTouches && e.targetTouches[0] && e.targetTouches[0].clientX) {
            x = e.targetTouches[0].clientX;
            y = e.targetTouches[0].clientY;
        } else if (e.clientX) {
            x = e.clientX;
            y = e.clientY;
        }
        return new Position(x, y);
    }
}

export class MessageUpdateEvent extends CustomEvent {
    talonSize;
    message;

    constructor() { super(MessageUpdateEvent.typeName); };

    static get typeName() {
        return "patience-message-update";
    }
}

class State {
    sourceStackId;
    index;
    targetStackId;

    element;
    eventPos;
    startPos;
    startAt;

    overDropable;

    toString() {
        return `State: sourceStackId=${this.sourceStackId} - index=${this.index} - sourceStackId=${this.targetStackId} - eventPos=${this.eventPos} -  + startPos=${this.startPos} - startAt=${this.startAt}overDropable=${this.overDropable}`;
    }
}

class Position {
    #x; #y;
    constructor(x, y) { this.#x = x; this.#y = y }
    get x() { return this.#x; }
    get y() { return this.#y; }
    toString() {
        return `Position: x=${this.x} y=${this.y}`;
    }
}

if (!customElements.get('patience-game')) {
    customElements.define('patience-game', Patience);
}
