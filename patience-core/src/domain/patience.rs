//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::ops::Range;
use core::slice::Iter;
use std::vec;

use super::*;
use super::card::*;
use super::column::*;
use super::stackpile::*;

#[derive(Debug)]
pub struct Patience {
    columns: HashMap<u8, Column>,
    stackpiles: HashMap<Suit, StackPile>,
    talon: Talon,
    discard_pile: DiscardPile,
    success_move_count: u32,
        move_count: u32
}

// Command part
impl Patience {

    const COLUMNS_AMOUNT: u8 = 7;
    const COLUMN_INDICES: Range<u8> = Range { start: 1, end: Self::COLUMNS_AMOUNT + 1 };

    pub fn new() -> Patience {

        let mut deck = Deck::shuffled_deck();
        let mut columns: HashMap<u8, Column> = HashMap::new();

        for column_count in Self::COLUMN_INDICES {
            let column = column_from_deck(column_count, &mut deck);
            columns.insert(column_count, column);
        }

        Patience {
            columns: columns,
            stackpiles: Patience::stackpile_map(),
            talon: talon_from_deck(&mut deck),
            discard_pile: DiscardPile::new(),
            success_move_count: 0,
            move_count: 0

        }
    }

    pub fn from_column_to_stackpile(&mut self, column_number: u8) -> Result<(), Option<&str>> {
        match self.columns.get_mut(&column_number) {
            Some(column) => {
                match column.top() {
                    Some(card) => {
                        let stackpile = self.stackpiles.get_mut(&card.suit).unwrap();
                        match column.move_top_to(stackpile) {
                            Ok(()) => Ok(()),
                            Err(message) => Err(message)
                        }
                    },
                    None => Err(Some("No top card in column"))
                }
            },
            None => Err(Some("No such column"))
        }
    }

    pub fn from_stackpile_to_column(&mut self, suit: &Suit, column_number: u8) -> Result<(), Option<&str>> {
        match self.columns.get_mut(&column_number) {
            Some(column) => {
                let suit = self.stackpiles.get_mut(suit).unwrap();
                match suit.move_top_to(column) {
                    Err(message) => Err(message),
                    Ok(()) => Ok(())
                }
            },
            None => Err(Some("No such column"))
        }
    }

    pub fn from_column_to_column(&mut self, cmd: &ColumnToColumnCommand) -> Result<(), Option<&str>> {

        if !self.columns.contains_key(&cmd.from_column_number) { return Err(Some("No from column")); }
        if !self.columns.contains_key(&cmd.to_column_number) { return Err(Some("No to column")); }

        if cmd.from_column_number == cmd.to_column_number { return Ok(()) }

        // The following code is one of the ugliest things I've done in a couple of years.
        // Problem: I can't get two mutable references from a HashMap. 
        //          As a solution I moved out the columns, do the operations an put the shit back.
        // What the hell did I miss and not understand?
        // After the first commit I found the following: https://users.rust-lang.org/t/borrowing-two-values-from-the-same-hashmap/48643
        // "remove, use, insert" seems to be a common pattern. But I need to understand RefCell<...> better.
        let mut from = self.columns.remove(&cmd.from_column_number).unwrap();
        let mut to = self.columns.remove(&cmd.to_column_number).unwrap();

        let result =  match cmd.card_index {
            None => from.move_top_to(&mut to),
            Some(index) => from.move_slice(index as usize, &mut to)
        };

        let retval = match result {
            Ok(_) => Ok(()),
            Err(_) => Err(Some("unable to move between columns"))
        };

        let _ = self.columns.insert(cmd.from_column_number, from);
        let _ = self.columns.insert(cmd.to_column_number, to);

        retval
    }

    pub fn from_discardpile_to_stackpile(&mut self) -> Result<(), Option<&str>> {
        match self.discard_pile.top() {
            None => Err(Some("No card in discard pile")),
            Some(card) => {
                match self.discard_pile.move_top_to(self.stackpiles.get_mut(&card.suit).unwrap()) {
                    Ok(()) => Ok(()),
                    Err(message) => Err(message)
                }
            }
        }
    }

    pub fn from_discardpile_to_column(&mut self, column_number: u8) -> Result<(), Option<&str>> {
        match self.columns.get_mut(&column_number) {
            Some(column) => match self.discard_pile.move_top_to(column) {
                Ok(()) => Ok(()),
                Err(message) => Err(message)
            },
            None => Err(Some("No such column"))
        }
    }

    pub fn from_talon_to_discardpile(&mut self) {
        if self.talon.is_empty() {
            discardpile_to_talon(&mut self.discard_pile, &mut self.talon);
        }
        match self.talon.move_top_to(&mut self.discard_pile) {
            _ => ()
        }
    }

    fn stackpile_map() -> HashMap<Suit, StackPile>{
        let mut stackpile_map: HashMap<Suit, StackPile> = HashMap::new();
        stackpile_map.insert(Suit::Spade, StackPile::new());
        stackpile_map.insert(Suit::Club, StackPile::new());
        stackpile_map.insert(Suit::Diamond, StackPile::new());
        stackpile_map.insert(Suit::Heart, StackPile::new());
        stackpile_map
    }

}


// The query part
impl Patience {

    pub fn cards_from_discard_pile(&self) ->  Iter<Card> {
        self.discard_pile.cards.iter()
    }

    pub fn cards_from_stackpile(&self, suit: &Suit) ->  Iter<Card> {
        self.stackpiles.get(suit).unwrap().cards.iter()
    }

    pub fn cards_from_column(&self, index: u8) ->  Option<Iter<Card>> {
        match self.columns.get(&index) {
            Some(column) => Some(column.cards.iter()),
            None => None
        }
    }

    pub fn talon_len(&self) -> usize {
        self.talon.len()
    }

    pub fn column_indices(&self) -> Range<u8> {
        Patience::COLUMN_INDICES
    }

    pub fn is_finished(&self) -> bool {
        for stackpile in self.stackpiles.values() {
            if !stackpile.is_finished() {
                return false;
            }
        }
        true
    }

    // sliced `true` represents a template card which is not a card on top
    // (e.g. from discard pile or stack pile). This can only happen to column cards.
    pub fn suggest_targets(&self, card: &Card, sliced: bool) -> Suggestions {

        if card.is_unknown() {
            return Suggestions::empty();
        }

        let mut suggested_column_indices: Vec<u8> = Vec::new();
        for index in self.column_indices() {
            if let Some(column) = self.columns.get(&index) {
                if column.can_suggest(card) {
                    suggested_column_indices.push(index);
                }
            }
        }

        let mut suits: Vec<Suit> = Vec::new();
        if !sliced {
            for (suit, stackpile) in &self.stackpiles {
                if suit == &card.suit && stackpile.can_suggest(card) {
                    suits.push(suit.clone());
                }
            }
        }

        Suggestions { column_indices: suggested_column_indices, suits }
    }

    /// Checks whether a Patience is guaranteed to be solvable or not.
    /// The implementation is conservative and returns a `false` in case of doubt.
    pub fn is_solvable(&self) -> bool {

        match  self.columns.values().map(|column| column.count_unknowns()).sum() {
            0 | 1 => true,
            _ => false
        }
    }
}

#[derive(Debug)]
pub struct Suggestions {
    pub column_indices: Vec<u8>,
    pub suits: Vec<Suit>
}

impl Suggestions {
    pub fn empty() -> Suggestions {
        Suggestions { column_indices: vec![], suits: vec![] }
    }
}

#[derive(Debug)]
pub struct ColumnToColumnCommand {
    pub from_column_number: u8,
    pub to_column_number: u8,
    /// A zero based index for bulk column to column moves.
    /// In case of `Option` is `None` a _top_ move will be done.
    pub card_index: Option<u8>
}


#[cfg(test)]
pub mod tests {

    use super::*;

    #[test]
    fn simple_column_to_stackpile() {
        // Given
        let mut patience = empty();
        assert!(patience.columns.get_mut(&1).unwrap().push(Card::create_unknown(KING, Suit::Spade)).is_ok());
        let ace: Card = Card::create_known(ACE, Suit::Spade);
        let _ = patience.columns.get_mut(&1).unwrap().push(ace);

        // When
        let result = patience.from_column_to_stackpile(1);

        // Then
        assert!(result.is_ok());
        assert_eq!(patience.stackpiles.get(&Suit::Spade).unwrap().cards.len(), 1);
        assert!(patience.stackpiles.get(&Suit::Club).unwrap().cards.is_empty());
        assert!(patience.stackpiles.get(&Suit::Heart).unwrap().cards.is_empty());
        assert!(patience.stackpiles.get(&Suit::Diamond).unwrap().cards.is_empty());
    }

    #[test]
    fn column_to_stackpile_form_illegal_card() {
        // Given
        let mut patience = empty();
        assert!(patience.columns.get_mut(&1).unwrap().push(Card::create_known(KING, Suit::Spade)).is_ok());

        // When
        let result = patience.from_column_to_stackpile(1);

        // Then
        assert!(result.is_err());
    }

    #[test]
    fn from_empty_column_to_stackpile() {
        // Given
        let mut patience = empty();

        // When
        let result = patience.from_column_to_stackpile(1);

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No top card in column");
    }

    #[test]
    fn from_none_existing_column_to_stackpile() {
        // Given
        let mut patience = empty();

        // When
        let result = patience.from_column_to_stackpile(0);

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No such column");
    }

    #[test]
    fn valid_card_from_discard_pile_to_stackpile() {
        // Given
        let mut patience = empty();
        let ace: Card = Card::create_unknown(ACE, Suit::Diamond);
        let _ = patience.discard_pile.push(ace);

        // When
        let result = patience.from_discardpile_to_stackpile();

        // Then
        assert!(result.is_ok());
        assert_eq!(patience.stackpiles.get(&Suit::Diamond).unwrap().top().unwrap().value, ACE);
    }

    #[test]
    fn invalid_card_from_discard_pile_to_stackpile() {
        // Given
        let mut patience = empty();
        let king: Card = Card::create_unknown(KING, Suit::Diamond);
        let _ = patience.discard_pile.push(king);

        // When
        let result = patience.from_discardpile_to_stackpile();

        // Then
        assert!(result.is_err());
        assert!(result.err().unwrap().is_none());
    }

    #[test]
    fn from_empty_discard_pile_to_stackpile() {
        // Given
        let mut patience = empty();

        // When
        let result = patience.from_discardpile_to_stackpile();

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No card in discard pile");
    }

    #[test]
    fn valid_from_discaprd_pile_to_column() {
        // Given
        let mut patience = empty();
        let king: Card = Card::create_unknown(KING, Suit::Club);
        let _ = patience.discard_pile.push(king);

        // When
        let result = patience.from_discardpile_to_column(2);

        // Then
        assert!(result.is_ok());
    }

    #[test]
    fn from_discaprd_pile_to_invalid_column() {
        // Given
        let mut patience = empty();
        let king: Card = Card::create_unknown(KING, Suit::Club);
        let _ = patience.discard_pile.push(king);

        // When
        let result = patience.from_discardpile_to_column(8);

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No such column");
    }

    #[test]
    fn invalid_from_discaprd_pile_to_column() {
        // Given
        let mut patience = empty();
        let ace: Card = Card::create_unknown(ACE, Suit::Club);
        let _ = patience.discard_pile.push(ace);

        // When
        let result = patience.from_discardpile_to_column(1);

        // Then
        assert!(result.is_err());
        assert!(result.err().unwrap().is_none());
    }

    #[test]
    fn from_talon_to_discard_pile() {
        // Given
        let mut patience = empty();
        let ace: Card = Card::create_known(ACE, Suit::Club);
        let _ = patience.talon.push(ace);

        // When
        patience.from_talon_to_discardpile();

        // Then
        assert_eq!(patience.discard_pile.top().unwrap().value, ACE);
        assert!(patience.discard_pile.top().unwrap().is_known());
    }

    #[test]
    fn from_top_column_to_column() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col1_card2: Card = Card::create_known(9, Suit::Club);
        let col1_card3: Card = Card::create_known(8, Suit::Heart);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2, col1_card3]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col2_card2: Card = Card::create_known(9, Suit::Spade);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        // When
        let cmd = ColumnToColumnCommand { from_column_number: 1, to_column_number: 2, card_index: None};
        let result = patience.from_column_to_column(&cmd);

        // Then
        assert!(result.is_ok());
        assert_eq!(patience.columns.get(&1).unwrap().top().unwrap().value, 9);
        assert_eq!(patience.columns.get(&2).unwrap().top().unwrap().value, 8);
    }

    #[test]
    fn from_column_slice_to_column() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col1_card2: Card = Card::create_known(9, Suit::Club);
        let col1_card3: Card = Card::create_known(8, Suit::Heart);
        let col1_card4: Card = Card::create_known(7, Suit::Spade);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2, col1_card3, col1_card4]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col2_card2: Card = Card::create_known(9, Suit::Spade);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        // When
        let cmd = ColumnToColumnCommand { from_column_number: 1, to_column_number: 2, card_index: Some(2)};
        let result = patience.from_column_to_column(&cmd);

        // Then
        assert!(result.is_ok());
        assert_eq!(patience.columns.get(&1).unwrap().cards.len(), 2);
        assert_eq!(patience.columns.get(&2).unwrap().cards.len(), 4);
    }

    #[test]
    fn to_same_column() {
        // Given
        let mut patience = empty();
 
        // When
        let cmd = ColumnToColumnCommand { from_column_number: 1, to_column_number: 1, card_index: Some(2)};
        let result = patience.from_column_to_column(&cmd);

        // Then
        assert!(result.is_ok());
    }

    #[test]
    fn from_invalid_column() {
        // Given
        let mut patience = empty();
 
        // When
        let cmd = ColumnToColumnCommand { from_column_number: 0, to_column_number: 1, card_index: Some(2)};
        let result = patience.from_column_to_column(&cmd);

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No from column");
    }

    #[test]
    fn column_to_invalid_column() {
        // Given
        let mut patience = empty();
 
        // When
        let cmd = ColumnToColumnCommand { from_column_number: 1, to_column_number: 8, card_index: Some(2)};
        let result = patience.from_column_to_column(&cmd);

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No to column");
    }

    #[test]
    fn simple_stackpile_to_column() {
        // Given
        let mut patience = empty();
        let col_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col_card1, col_card2]);

        let suit_card1: Card = Card::create_known(ACE, Suit::Club);
        let suit_card2: Card = Card::create_known(2, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_card1, suit_card2]);

        // When
        let result = patience.from_stackpile_to_column(&Suit::Club, 1);

        // Then
        assert!(result.is_ok());
        assert_eq!(patience.stackpiles.get(&Suit::Club).unwrap().top().unwrap().value, ACE);
        assert_eq!(patience.columns.get(&1).unwrap().top().unwrap().value, 2);
    }

    #[test]
    fn from_stackpile_to_illegal_column() {
        // Given
        let mut patience = empty();

        // When
        let result = patience.from_stackpile_to_column(&Suit::Club, 8);

        // Then
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().unwrap(), "No such column");
    }

    #[test]
    fn simple_solvable() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        assert!(patience.is_solvable());
    }

    #[test]
    fn simple_not_solvable() {
        // Given
        let mut patience: Patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Heart);
        let col2_card2: Card = Card::create_known(3, Suit::Club);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        assert!(!patience.is_solvable());
    }

    #[test]
    fn test_query_column_indices_for_len() {
        // Given
        let patience: Patience = empty();

        // When
        let indices = patience.column_indices();

        // Then
        assert_eq!(indices.len(), 7);
    }

    #[test]
    fn test_query_column_indices_for_last() {
        // Given
        let patience: Patience = empty();

        // When
        let indices = patience.column_indices();

        // Then
        assert_eq!(indices.last().unwrap(), 7);
    }

    #[test]
    fn test_query_column_indices_for_first() {
        // Given
        let patience: Patience = empty();

        // When
        let indices = patience.column_indices();

        // Then
        assert_eq!(indices.into_iter().next().unwrap(), 1);
    }

    #[test]
    fn simple_suggest() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Heart);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let col3_card1: Card = Card::create_unknown(KING, Suit::Diamond);
        let col3_card2: Card = Card::create_known(3, Suit::Spade);
        fill_in_column(&mut patience, 3, vec![col3_card1, col3_card2]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1]);
        let suit_spade_card1: Card = Card::create_known(ACE, Suit::Spade);
        fill_in_stackpile(&mut patience, &Suit::Spade, vec![suit_spade_card1]);

        let template: Card = Card::create_known(2, Suit::Club);

        // When
        let result = patience.suggest_targets(&template, false);

        // Then
        assert_eq!(result.column_indices.len(), 2);
        assert!(result.column_indices.contains(&1));
        assert!(result.column_indices.contains(&2));

        assert_eq!(result.suits.len(), 1);
        assert_eq!(result.suits[0], Suit::Club);

    }

    #[test]
    fn sliced_suggest() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Heart);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let col3_card1: Card = Card::create_unknown(KING, Suit::Diamond);
        let col3_card2: Card = Card::create_known(3, Suit::Spade);
        fill_in_column(&mut patience, 3, vec![col3_card1, col3_card2]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1]);
        let suit_spade_card1: Card = Card::create_known(ACE, Suit::Spade);
        fill_in_stackpile(&mut patience, &Suit::Spade, vec![suit_spade_card1]);

        let template: Card = Card::create_known(2, Suit::Club);

        // When
        let result = patience.suggest_targets(&template, true);

        // Then
        assert_eq!(result.column_indices.len(), 2);
        assert!(result.column_indices.contains(&1));
        assert!(result.column_indices.contains(&2));

        assert!(result.suits.is_empty());

    }

    #[test]
    fn suggest_target_with_no_matching_card() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let col2_card1: Card = Card::create_unknown(KING, Suit::Club);
        let col2_card2: Card = Card::create_known(3, Suit::Heart);
        fill_in_column(&mut patience, 2, vec![col2_card1, col2_card2]);

        let suit_club_card1: Card = Card::create_known(ACE, Suit::Club);
        fill_in_stackpile(&mut patience, &Suit::Club, vec![suit_club_card1]);

        let template: Card = Card::create_known(3, Suit::Club);

        // When
        let result = patience.suggest_targets(&template, false);

        // Then
        assert!(result.column_indices.is_empty());
        assert!(result.suits.is_empty());

    }

    #[test]
    fn simple_suggest_with_unknown_template() {
        // Given
        let mut patience = empty();
        let col1_card1: Card = Card::create_unknown(KING, Suit::Spade);
        let col1_card2: Card = Card::create_known(3, Suit::Diamond);
        fill_in_column(&mut patience, 1, vec![col1_card1, col1_card2]);

        let template: Card = Card::create_unknown(12, Suit::Heart);

        // When
        let result = patience.suggest_targets(&template, false);

        // Then
        assert_eq!(result.column_indices.len(), 0);
        assert_eq!(result.suits.len(), 0);
    }

    pub fn fill_in_stackpile(patience: &mut Patience, suit: &Suit, cards: Vec<Card>) {
        let suit = patience.stackpiles.get_mut(suit).unwrap();
        for card in cards {
            let _ = suit.push(card);
        }
    }

    pub fn fill_in_column(patience: &mut Patience, column_index: u8, cards: Vec<Card>) {
        let column = patience.columns.get_mut(&column_index).unwrap();
        for card in cards {
            let _ = column.push(card);
        }
    }

    pub fn fill_in_discard_pile(patience: &mut Patience, cards: Vec<Card>) {
        for card in cards {
            let _ = patience.discard_pile.push(card);
        }
    }

    // Currently for test only
    pub fn empty() -> Patience {
        let mut columns: HashMap<u8, Column> = HashMap::new();
        for column_count in Patience::COLUMN_INDICES {
            columns.insert(column_count, Column::new());
        }
        Patience {
            columns: columns,
            stackpiles: Patience::stackpile_map(),
            talon: Talon::new(),
            discard_pile: DiscardPile::new()
        }
    }

}
