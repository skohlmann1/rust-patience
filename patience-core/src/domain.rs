//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::vec::*;
use rand::thread_rng;
use rand::seq::SliceRandom;

pub mod card;
pub mod column;
pub mod stackpile;
pub mod patience;

use self::card::*;
use self::column::*;


#[derive(Debug)]
pub struct Talon {
    cards: Vec<Card>,
}

impl Talon {
    pub fn new() -> Talon {
        Talon {
            cards: Vec::new()
        }
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    pub fn len(&self) -> usize {
        self.cards.len()
    }
}

#[derive(Debug)]
pub struct DiscardPile {
    cards: Vec<Card>
}

impl DiscardPile {
    pub fn new() -> DiscardPile {
        DiscardPile {
            cards: Vec::new()
        }
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }
}

/// Implementations return the top card reference (e.g the last pushed card to a card stack)
/// if the card is known.
pub trait KnownTopStack {
    fn top(&self) -> Option<&Card>;
}

impl KnownTopStack for DiscardPile {
    fn top(&self) -> Option<&Card> {
        self.cards.last()
    }
}

/// Implementations push a card to the top of the stack if and only if this is possible.
/// 
/// In case a push is not possible the given card will be returned within the failure result.
pub trait CardStack {
    fn push(&mut self, card: Card) -> Result<(), (Card, Option<&str>)>;
}

/// Implementation checks wether a card is pushable or not.
pub trait Suggestionable {
    /// Use the function e.g. for hints
    fn can_suggest(&self, card: &Card) -> bool;
}

impl CardStack for DiscardPile {

    fn push(&mut self, card: Card) -> Result<(), (Card, Option<&str>)> {
        if card.is_known() {
            return Err((card, None))
        }

        let known = Card::to_known(card);
        self.cards.push(known);

        Ok(())
    }
}

impl CardStack for Talon {

    fn push(&mut self, card: Card) -> Result<(), (Card, Option<&str>)> {
        let unknown = Card::to_unknown(card);
        self.cards.push(unknown);

        Ok(())
    }
}


pub trait TopMoveable {
    fn move_top_to<CS: CardStack>(&mut self, card_stack: &mut CS) -> Result<(), Option<&str>>;
}

impl TopMoveable for DiscardPile {
    fn move_top_to<CS: CardStack>(&mut self, card_stack: &mut CS) -> Result<(), Option<&str>> {
        match self.cards.pop() {
            Some(card) => {
                match card_stack.push(card) {
                    Ok(_) => Ok(()),
                    Err((bc, _f)) => {
                        self.cards.push(bc);
                        Err(None)
                    }
                }
            },
            None => {
                Err(None)
            }
        }
    }
}

impl TopMoveable for Talon {
    fn move_top_to<CS: CardStack>(&mut self, card_stack: &mut CS) -> Result<(), Option<&str>> {
        match self.cards.pop() {
            Some(card) => {
                match card_stack.push(card) {
                    Err((bc, _f)) => {
                        self.cards.push(bc);
                        Err(None)
                    }
                    _ => Ok(())
                }
            },
            None => {
                Err(None)
            }
        }
    }
}

pub struct Deck {
    pub cards: Vec<Card>
}

impl Deck {
    pub fn shuffled_deck() -> Self {
        let mut deck_data: Vec<Card> = Vec::with_capacity(13 * 4);
        let mut start = 1;

        while start <= 13 {
            deck_data.push(Card::create_unknown(start, Suit::Spade));
            deck_data.push(Card::create_unknown(start, Suit::Heart));
            deck_data.push(Card::create_unknown(start, Suit::Diamond));
            deck_data.push(Card::create_unknown(start, Suit::Club));

            start += 1;
        }

        deck_data.shuffle(&mut thread_rng());

        Deck {
            cards: deck_data
        }
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    pub fn next(&mut self) -> Option<Card> {
        self.cards.pop()
    }
}

pub fn column_from_deck(len: u8, deck: &mut Deck) -> Column {
    let mut column = Column::new();

    for _ in 0..len {
        match deck.next() {
            Some(card) => match column.push(card) {
                Err((_, _)) => panic!("WTF happened!!!"),
                _ => ()
            },
            _ => panic!("Deck out of cards")
        }
    }

    column.make_top_known();
    column
}

pub fn talon_from_deck(deck: &mut Deck) -> Talon {
    let mut talon = Talon::new();
    while !deck.is_empty() {
        if let Some(card) = deck.next() {
            if let Err((_, _)) =  talon.push(card) {
                panic!("WTF happened!!!");
            }
        }
    }

    talon
}

pub fn discardpile_to_talon(discard_pile: &mut DiscardPile, talon: &mut Talon) {
    while !discard_pile.is_empty() {
        match discard_pile.move_top_to(talon) {
            _ => ()
        }
    }
}


#[cfg(test)]
pub mod tests {
    use crate::domain::*;
    use crate::domain::stackpile::*;


    #[test]
    fn accepted_move_top_card_from_column_to_stackpile() {
        let ace: Card = Card::create_known(1, Suit::Spade);
        let mut column = Column::new();
        column.cards.push(ace);
        assert!(!column.cards.is_empty());

        let stackpile = &mut StackPile::new();  // WTF!
        assert_eq!(stackpile.cards.len(), 0);
        let _ = column.move_top_to(stackpile); 

        assert!(column.cards.is_empty());
        assert_eq!(stackpile.cards.len(), 1);
    }

    #[test]
    fn rejected_move_top_card_from_column_to_stackpile_and_make_new_top_known() {
        let king: Card = Card::create_unknown(KING, Suit::Spade);
        let ace: Card = Card::create_known(ACE, Suit::Spade);
        let mut column = Column::new();
        column.cards.push(king);
        column.cards.push(ace);

        let stackpile = &mut StackPile::new();
        assert_eq!(stackpile.cards.len(), 0);
        let _ = column.move_top_to(stackpile); 

        assert!(column.is_top_known());
        assert_eq!(column.top().unwrap().value, KING);
        assert_eq!(stackpile.cards.len(), 1);
    }

    #[test]
    fn accepted_move_top_card_from_stackpile_to_column() {
        let mut stackpile = StackPile::new();
        let ace: Card = Card::create_known(ACE, Suit::Spade);
        let _ = stackpile.push(ace);
        assert_eq!(stackpile.top().unwrap().suit, Suit::Spade);

        let column = &mut Column::new();
        let two: Card = Card::create_unknown(2, Suit::Heart);
        let _ = column.push(two);
        assert!(column.top().unwrap().is_unknown());
        column.make_top_known();
        assert!(column.top().unwrap().is_known());

        let _ = stackpile.move_top_to(column);
        assert!(stackpile.top().is_none());
        assert_eq!(column.top().unwrap().suit, Suit::Spade);
    }

    #[test]
    fn rejected_move_top_card_from_stackpile_to_column() {
        let mut stackpile = StackPile::new();
        let ace: Card = Card::create_known(ACE, Suit::Spade);
        let _ = stackpile.push(ace);

        let column = &mut Column::new();
        let three: Card = Card::create_unknown(3, Suit::Heart);
        let _ = column.push(three);
        let _ = column.make_top_known();

        let _ = stackpile.move_top_to(column);
        assert_eq!(column.top().unwrap().suit, Suit::Heart);
        assert_eq!(stackpile.top().unwrap().suit, Suit::Spade);
    }

    #[test]
    fn move_from_talon_to_discardpile() {
        let talon = &mut Talon::new();
        let _ = talon.push(Card::create_unknown(3, Suit::Diamond));
        let _ = talon.push(Card::create_unknown(ACE, Suit::Heart));

        let discard_pile = &mut DiscardPile::new();
        talon.move_top_to(discard_pile).unwrap();
        talon.move_top_to(discard_pile).unwrap();

        assert!(talon.is_empty());
        assert!(discard_pile.top().unwrap().is_known());
        assert_eq!(discard_pile.top().unwrap().value, 3);
    }

    #[test]
    fn move_from_talon_to_discardpile_and_back() {
        let talon = &mut Talon::new();
        let _ = talon.push(Card::create_unknown(3, Suit::Diamond));
        let _ = talon.push(Card::create_unknown(1, Suit::Heart));

        let discard_pile = &mut DiscardPile::new();
        talon.move_top_to(discard_pile).unwrap();
        talon.move_top_to(discard_pile).unwrap();

        discard_pile.move_top_to(talon).unwrap();
        discard_pile.move_top_to(talon).unwrap();

        assert!(discard_pile.is_empty());
        assert!(talon.cards.last().unwrap().is_unknown());
        assert!(talon.cards.first().unwrap().is_unknown());
        assert_eq!(talon.cards.last().unwrap().value, ACE);
        assert_eq!(talon.cards.first().unwrap().value, 3);
    }

    #[test]
    fn all_talon_from_deck() {
        let deck = &mut Deck::shuffled_deck();
        let talon = talon_from_deck(deck);

        assert_eq!(talon.cards.len(), 52);

    }

    #[test]
    fn single_card_column_from_deck() {
        let deck = &mut Deck::shuffled_deck();
        let column = column_from_deck(1, deck);

        assert_eq!(column.cards.len(), 1);
        assert!(column.top().unwrap().is_known());
    }

    #[test]
    fn two_cards_column_from_deck() {
        let deck = &mut Deck::shuffled_deck();
        let column = column_from_deck(2, deck);

        assert_eq!(column.cards.len(), 2);
        assert!(column.cards[0].is_unknown());
        assert!(column.top().unwrap().is_known());
    }
}
