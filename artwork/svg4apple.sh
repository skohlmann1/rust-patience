#!/usr/bin/env bash
#
# Generates PNGs for apple, scaled
#
# Usage example:
#
#     find . -name "*.svg" -exec svg4apple.sh {} png \;
#
# 
# Parameter
#
# - first  : name of the SVG file
# - second : folder to store the PNG images

set +o nounset

function source_dir {
    local SOURCE="${BASH_SOURCE[0]}"
    DIR="$( dirname "$SOURCE" )"
    while [ -h "$SOURCE" ]
    do
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
        DIR="$( cd -P "$( dirname "$SOURCE"  )" && pwd )"
    done
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

source_dir


file="${1}"
_tmp=$(basename "${file}")
name="${_tmp%.*}"
target_dir="${2}"

if [ ! -d "${target_dir}" ]; then
	mkdir -p "${target_dir}"
fi

"${DIR}/svg2png.sh" -w 100 -o "${target_dir}/${name}@1.png" "${file}"
"${DIR}/svg2png.sh" -w 200 -o "${target_dir}/${name}@2.png" "${file}"
"${DIR}/svg2png.sh" -w 300 -o "${target_dir}/${name}@3.png" "${file}"

