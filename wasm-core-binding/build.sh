#!/usr/bin/env bash

function prompt_confirm() {
    while true; do
    	read -r -n 1 -p "${1:-Continue?} [y/n]: " REPLY
	    case $REPLY in
        	[yY]) echo ; return 0 ;;
        	[nN]) echo ; return 1 ;;
      		*) echo "invalid input" ;;
    	esac
  	done  
}

function try_install_wasm_pack() {
    if [ ! $(type -p "curl") ]; then
        echo "'curl' not found."
        echo "'curl' must be available to install 'wasm-pack'. I give up."
        exit 3
    fi
    curl \https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
}

if [ ! $(type -p "wasm-pack") ]; then
    echo "'wasm-pack' not found."
    echo "Should I install 'wasm-pack'?"
    prompt_confirm
    if [ "${?}" = 0 ]; then
        try_install_wasm_pack
    else
        echo "You declined to install 'wasm-pack'."
        echo "Without building the project is not possible."
        echo "Bye"
        exit 2
    fi
fi

echo "Start building Rust Patience WASM Core binding"
wasm-pack build --target web
