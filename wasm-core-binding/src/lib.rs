//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! WASM wrapper for [application::Game](patience_core::application::Game)
extern crate wasm_bindgen;

use patience_core::application::*;
use wasm_bindgen::prelude::*;

/// WASM wrapper for [application::Game](patience_core::application::Game)
#[wasm_bindgen]
pub struct Game {
    game: patience_core::application::Game
}

#[wasm_bindgen]
impl Game {

    /// WASM wrapper for [application::Game::new()](patience_core::application::Game::new())
    pub fn new() -> Game {
        Game {
            game: patience_core::application::Game::new()
        }
    }

    /// WASM wrapper for [application::Game::do_move()](patience_core::application::Game::do_move)
    /// 
    /// An empty `target` is wrapped to `MoveCommand::target: None`.
    pub fn do_move(&mut self, start: String, index: Option<u32>, target: &str) -> Result<(), Option<String>> {
        let mut cmd = MoveCommand::builder(start.as_str());
        cmd.index = index;
        cmd.target = if target.trim().is_empty() {
            None
        } else {
            Some(target)
        };

        let result = self.game.do_move(&cmd);
        match result {
            Ok(()) => Ok(()),
            Err(msg) => match msg {
                None => Err(None),
                Some(errmsg) => Err(Some(String::from(errmsg)))
            }
        }
    }

    /// WASM wrapper for [application::Game::query_talon_len](patience_core::application::Game::query_talon_len)
    pub fn query_talon_len(&self) -> usize {
        self.game.query_talon_len()
    }

    /// WASM wrapper for [application::Game::query_is_finished](patience_core::application::Game::query_is_finished)
    pub fn query_is_finished(&self) -> bool {
        self.game.query_is_finished()
    }

    /// WASM wrapper for [application::Game::query_is_solvable](patience_core::application::Game::query_is_solvable)
    pub fn query_is_solvable(&self) -> bool {
        self.game.query_is_solvable()
    }

    /// WASM wrapper for [application::Game::query_discardpile](patience_core::application::Game::query_discardpile)
    pub fn query_discardpile(&self, all: bool) -> Vec<CardSymbols> {
        self.game.query_discardpile(&QueryDiscardPile{all})
            .iter()
            .map(|cs| CardSymbols::from(cs))
            .collect()
    }

    /// WASM wrapper for [application::Game::query_column_indices](patience_core::application::Game::query_column_indices)
    pub fn query_column_indices(&self) -> Vec<u8> {
        self.game.query_column_indices()
    }

    /// WASM wrapper for [application::Game::query_column](patience_core::application::Game::query_column)
    pub fn query_column(&self, index: u8) -> Vec<CardSymbols> {
        self.game.query_column(&QueryColumn {column_index: index })
            .iter()
            .map(|cs| CardSymbols::from(cs))
            .collect()
    }

    /// WASM wrapper for [application::Game::query_stackpile](patience_core::application::Game::query_stackpile)
    pub fn query_stackpile(&self, suit: char) -> Option<Vec<CardSymbols>> {
        match self.game.query_stackpile(&QueryStackPile {suit}) {
            Some(css) => Some(css.iter().map(|cs| CardSymbols::from(cs)).collect()),
            _ => None
        }
    }

    /// WASM wrapper for [application::Game::query_suggestions](patience_core::application::Game::query_suggestions)
    pub fn query_suggestions(&self, start: &str, index: Option<u32>) -> Vec<MoveSuggestion> {
        let suggestions = self.game.query_suggestions(&QuerySuggestion{ start, index});
        let mut retval: Vec<MoveSuggestion> = Vec::new();
        for suggestion in suggestions {
            retval.push(MoveSuggestion::new(suggestion.start, suggestion.index, suggestion.target));
        }
        retval
    }
}

/// WASM wrapper for [application::Suggestion](patience_core::application::Suggestion)
#[wasm_bindgen]
pub struct MoveSuggestion {
    start: String,
    index: Option<u32>,
    target: String
}

#[wasm_bindgen]
impl MoveSuggestion {

    // According to [wasm-bindgen#1985](https://github.com/rustwasm/wasm-bindgen/issues/1985),
    // public fields of structs are required to be `Copy` in order for the automatically
    // generated accessors to function. As `String` has no `Copy` implementation creating
    // getter with cloning is the solution.
    // `Option` does support the `Copy` trait. However, I want a uniform interface.
    // For this reason, `index` is also delivered via a getter implementation.

    #[wasm_bindgen(constructor)]
    pub fn new(start: String, index: Option<u32>, target: String) -> MoveSuggestion {
        MoveSuggestion { start, index, target }
    }    
    
    #[wasm_bindgen(getter)]
    pub fn start(&self) -> String {
        self.start.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn index(&self) -> Option<u32> {
        self.index.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn target(&self) -> String {
        self.target.clone()
    }
}


/// WASM wrapper for [application::CardSymbols](patience_core::application::CardSymbols)
#[wasm_bindgen]
pub struct CardSymbols {
    pub card_symbol: char,
    pub suit_symbol: Option<char>,
    pub value: Option<u32>
}

impl CardSymbols {
    fn from(cs: &patience_core::application::CardSymbols) -> CardSymbols {
        CardSymbols {
            card_symbol: cs.card_symbol,
            suit_symbol: cs.suit_symbol,
            value: cs.value
        }
    }
}
