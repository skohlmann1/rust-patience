//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! C wrapper for [application::Game](patience_core::application::Game)

use patience_core::application::*;
use std::ffi::{CString, CStr, c_char, c_uint, c_uchar};
use std::ptr;
use std::mem;

/// Defines a successful [patience_do_move] command execution
pub const PATIENCE_DO_MOVE_OK: c_uint = 0;
/// Defines an unsuccessful [patience_do_move] command execution where the `start` value is not given (e.g. `null`).
pub const PATIENCE_DO_MOVE_NO_START: c_uint = 1;
/// Defines an unsuccessful [patience_do_move] command execution.
pub const PATIENCE_DO_MOVE_NOT_POSSIBLE: c_uint = 2;
/// The game reference pointer is not delivered to the [patience_do_move] function.
pub const PATIENCE_DO_MOVE_NO_GAME_REFERENCE: c_uint = 3;
/// An unknown error occured during the [patience_do_move] command execution.
pub const PATIENCE_DO_MOVE_UNKNOWN_ERR: c_uint = 255;

/// Creates an instance of [application::Game](patience_core::application::Game)
/// and returns a pointer to.
/// 
/// Free the instance with `patience_destroy_game`.
#[no_mangle]
pub extern "C" fn patience_create_game() -> *mut Box<Game> {
    let instance = Game::new();
    let bx = Box::new(instance);
    Box::into_raw(bx.into())
}

/// Performs a card move.
/// 
/// See `PATIENCE_DO_MOVE_` for possible return values.
/// 
/// Behavior is not defined if `game_ref` is a `null` pointer.
/// 
/// * `game_ref` - The game reference from [patience_create_game]
/// * `start` - The start value. Never `null`. E.g. "dp" or "c1" or a suit name.
/// * `index` - The index value for a stacked column to column move. If no index is given, the value must be zero (0).
/// * `target` - The start pile. Might be `null` for the _talon_ to _discard pile_ move.
#[no_mangle]
pub extern "C" fn patience_do_move(game_ref: *mut Box<Game>, start: *const c_char, index: c_uint, target: *const c_char) -> c_uint {
    if game_ref.is_null() { return PATIENCE_DO_MOVE_NO_GAME_REFERENCE }

    unsafe {
        if start.is_null() {
            return PATIENCE_DO_MOVE_NO_START
        }

        let start_str;
        if let Ok(str) = CStr::from_ptr(start).to_str() {
            start_str = str;
        } else {
            return PATIENCE_DO_MOVE_UNKNOWN_ERR
        }

        let column_index;
        if index == 0 {
            column_index = None;
        } else {
            column_index = Some(index);
        }

        let target_str;
        if !target.is_null() {
            if let Ok(str) = CStr::from_ptr(target).to_str() {
                target_str = Some(str);
            } else {
                target_str = None;
            }
        } else {
            target_str = None;
        }

        let move_cmd = MoveCommand {
            start: start_str,
            index: column_index,
            target: target_str
        };

        let instance = &mut *game_ref;
        match instance.do_move(&move_cmd) {
            Ok(()) => PATIENCE_DO_MOVE_OK,
            Err(_) => PATIENCE_DO_MOVE_NOT_POSSIBLE
        }
    }
}


/// Behavior is not defined if `game_ref` is a `null` pointer.
#[no_mangle]
pub extern "C" fn patience_query_talon_len(game_ref: *mut Box<Game>) -> usize {
    if game_ref.is_null() { return 0 }
    unsafe {
        let instance = &mut *game_ref;
        instance.query_talon_len()
    }
}

/// Delivers an array of discapard pile PatienceCardSymbols
/// 
/// To get a dedicated card call `patience_card_symbol_from_array` with the result of this function and the index of the card.
/// 
/// Behavior is not defined if `game_ref` is a `null` pointer.
/// 
/// If `game_ref` is not `null` the return value is never `null` and must be destroyed with `patience_destroy_card_symbols_array`
#[no_mangle]
pub extern "C" fn patience_query_discardpile(game_ref: *mut Box<Game>, all: bool) -> *mut Box<PatienceCardSymbolsArray> {
    if game_ref.is_null() { return ptr::null_mut() }
    unsafe {
        let game = &mut *game_ref;
        let dp = game.query_discardpile( &QueryDiscardPile {all} );
        card_vec_to_patience_card_symbols_array_reference(dp)
    }
}

/// Delivers an array of stackpile PatienceCardSymbols
/// 
/// To get a dedicated card call `patience_card_symbol_from_array` with the result of this function and the index of the card.
/// 
/// Behavior is not defined if `game_ref` is a `null` pointer.
/// 
/// If `game_ref` is not `null` the return value is never `null` and must be destroyed with `patience_destroy_card_symbols_array`
#[no_mangle]
pub extern "C" fn patience_query_stackpile(game_ref: *mut Box<Game>, suit: c_char) -> *mut Box<PatienceCardSymbolsArray> {
    if game_ref.is_null() { return ptr::null_mut() }
    unsafe {
        let game = &mut *game_ref;
        let sp_opt = game.query_stackpile( &QueryStackPile {suit: suit.to_string().pop().unwrap_or('u')} );
        match sp_opt {
            None => ptr::null_mut(),
            Some(sp) => card_vec_to_patience_card_symbols_array_reference(sp)
        }
    }
}

/// Delivers an array of column PatienceCardSymbols
/// 
/// To get a dedicated card call `patience_card_symbol_from_array` with the result of this function and the index of the card.
/// 
/// Behavior is not defined if `game_ref` is a `null` pointer.
/// 
/// If `game_ref` is not `null` the return value is never `null` and must be destroyed with `patience_destroy_card_symbols_array`
#[no_mangle]
pub extern "C" fn patience_query_column(game_ref: *mut Box<Game>, column_index: c_uchar) -> *mut Box<PatienceCardSymbolsArray> {
    if game_ref.is_null() { return ptr::null_mut() }
    unsafe {
        let game = &mut *game_ref;
        let sp = game.query_column( &QueryColumn { column_index } );
        card_vec_to_patience_card_symbols_array_reference(sp)
    }
}

/// Behavior is not defined if `game_ref` is a `null` pointer.
#[no_mangle]
pub extern "C" fn patience_query_is_solvable(game_ref: *mut Box<Game>) -> bool {
    unsafe {
        let game = &mut *game_ref;
        game.query_is_solvable()
    }
}

/// Behavior is not defined if `game_ref` is a `null` pointer.
#[no_mangle]
pub extern "C" fn patience_query_is_finished(game_ref: *mut Box<Game>) -> bool {
    unsafe {
        let game = &mut *game_ref;
        game.query_is_finished()
    }
}

/// Delivers an array of column indices.
/// 
/// Column indexes always starts with 1.
/// 
/// Behavior is not defined if `game_ref` is a `null` pointer.
/// 
/// Returned pointer is never `null` and reference must be destroyed with `patience_destroy_column_indices`.
#[no_mangle]
pub extern "C" fn patience_query_column_indices(game_ref: *mut Box<Game>) -> *mut Box<ColumnIndices> {
    unsafe {
        let game = &mut *game_ref;
        let mut vec = game.query_column_indices().into_boxed_slice();
        let vec_data = vec.as_mut_ptr();
        let len = vec.len();

        mem::forget(vec);

        let retval =  Box::new(ColumnIndices { len, indices: vec_data });

        Box::into_raw(retval.into())
    }
}

/// Returns the value of the given index.
/// 
/// If index is not reachable the return value is 0 otherwise between 1 and n.
#[no_mangle]
pub extern "C" fn patience_column_index_from_column_indices(indices_ref: *mut Box<ColumnIndices>, index: usize) -> c_uchar {
    if indices_ref.is_null() { return 0 }
    unsafe {
        let indices: &mut Box<ColumnIndices> = &mut *indices_ref;
        if indices.indices.is_null() {
            return 0
        }

        let elements = ptr::slice_from_raw_parts_mut(indices.indices, indices.len);
        let element_slice = &mut *elements;
        match element_slice.get(index as usize) {
            None => 0,
            Some(val) => *val
        }
    }
}

/// Delivers a PatienceCardSymbols from the given array.
/// 
/// * `array_ref` - a reference to the card symbol array
/// * `index` - the index of the card to deliver. Starting with 0.
/// 
/// Behavior is not defined if `array_ref` is a `null` pointer.
/// 
/// If `array_ref` is not `null` the return value is never `null`.
#[no_mangle]
pub extern "C" fn patience_card_symbol_from_array(array_ref: *mut Box<PatienceCardSymbolsArray>, index: usize) -> *mut PatienceCardSymbols {
    if array_ref.is_null() { return ptr::null_mut() }
    unsafe {
        let array: &mut Box<PatienceCardSymbolsArray> = &mut *array_ref;
        if array.array.is_null() {
            return ptr::null_mut()
        }
        let elements = ptr::slice_from_raw_parts_mut(array.array, array.array_len);
        let element_slice = &mut *elements;
        match element_slice.get_mut(index) {
            None => ptr::null_mut(),
            Some(pcs) => ptr::from_mut(pcs)
        }
    }
}


/// Delivers an array of PatienceMoveSuggestions.
/// 
/// To get a dedicated card call `patience_move_suggestion_from_array` with the result of this function and the index of the card.
/// 
/// Behavior is not defined if `game_ref` is a `null` pointer.
/// 
/// If `game_ref` is not `null` and `start` is not `null` the return value is never `null` 
/// and must be destroyed with `patience_destroy_move_suggestion_array`
#[no_mangle]
pub extern "C" fn patience_query_suggestions(game_ref: *mut Box<Game>, start: *const c_char, index: c_uint) -> *mut Box<PatienceMoveSuggestionArray> {
    if game_ref.is_null() { return ptr::null_mut(); }

    unsafe {
        if start.is_null() {
            return ptr::null_mut();
        }

        let start_str;
        if let Ok(str) = CStr::from_ptr(start).to_str() {
            start_str = str;
        } else {
            return ptr::null_mut();
        }

        let column_index;
        if index == 0 {
            column_index = None;
        } else {
            column_index = Some(index);
        }

        let query_suggestion = QuerySuggestion {
            start: start_str,
            index: column_index
        };

        let instance = &mut *game_ref;
        let ms_vec = instance.query_suggestions( &query_suggestion );


        let mut pms: Vec<PatienceMoveSuggestion> = ms_vec.iter()
            .map(|ms| PatienceMoveSuggestion::from(ms))
            .collect();
        pms.shrink_to_fit();
        let array_len = pms.len();

        let mut boxed_slice = pms.into_boxed_slice();
        let array: *mut PatienceMoveSuggestion = boxed_slice.as_mut_ptr();

        mem::forget(boxed_slice);

        let retval =  Box::new(PatienceMoveSuggestionArray {
            array,
            array_len
        });

        Box::into_raw(retval.into())
    }
}

/// Delivers a PatienceMoveSuggestion from the given array.
/// 
/// * `array_ref` - a reference to the card symbol array
/// * `index` - the index of the card to deliver. Starting with 0.
/// 
/// Behavior is not defined if `array_ref` is a `null` pointer.
/// 
/// If `array_ref` is not `null` the return value is never `null`.
#[no_mangle]
pub extern "C" fn patience_move_suggestion_from_array(array_ref: *mut Box<PatienceMoveSuggestionArray>, index: usize) -> *mut PatienceMoveSuggestion {
    if array_ref.is_null() { return ptr::null_mut() }
    unsafe {
        let array: &mut Box<PatienceMoveSuggestionArray> = &mut *array_ref;
        if array.array.is_null() {
            return ptr::null_mut();
        }
        let elements = ptr::slice_from_raw_parts_mut(array.array, array.array_len);
        let element_slice = &mut *elements;
        match element_slice.get_mut(index) {
            None => ptr::null_mut(),
            Some(pms) => ptr::from_mut(pms)
        }
    }
}


#[no_mangle]
pub extern "C" fn patience_destroy_game(game_ref: *mut Box<Game>) {
    if game_ref.is_null() { return  }

    unsafe { drop(Box::from_raw(game_ref)); }
}

#[no_mangle]
pub extern "C" fn patience_destroy_card_symbols_array(array_ref: *mut Box<PatienceCardSymbolsArray>) {
    if array_ref.is_null() { return }
    unsafe { 
        let instance = &mut *array_ref;
        if !instance.array.is_null() {
            let elements = ptr::slice_from_raw_parts_mut(instance.array, instance.array_len);
            let element_slice = &mut *elements;

            for element in element_slice {
                if !element.card_symbol.is_null() {
                    drop(CString::from(CStr::from_ptr(element.card_symbol)));
                }
                if !element.suit_symbol.is_null() {
                    drop(CString::from(CStr::from_ptr(element.suit_symbol)));
                }
            }

            // drop(Box::from_raw(instance.array));
        }

        drop(Box::from_raw(array_ref)); 
    }
}

#[no_mangle]
pub extern "C" fn patience_destroy_move_suggestion_array(array_ref: *mut Box<PatienceMoveSuggestionArray>) {
    if array_ref.is_null() { return }
    unsafe { 
        let instance = &mut *array_ref;
        if !instance.array.is_null() {
            let elements = ptr::slice_from_raw_parts_mut(instance.array, instance.array_len);
            let element_slice = &mut *elements;

            for element in element_slice {
                if !element.start.is_null() {
                    drop(CString::from(CStr::from_ptr(element.start)));
                }
                if !element.target.is_null() {
                    drop(CString::from(CStr::from_ptr(element.target)));
                }
            }

            // drop(Box::from_raw(instance.array));
        }

        drop(Box::from_raw(array_ref)); 
    }
}

#[no_mangle]
pub extern "C" fn patience_destroy_column_indices(array_ref: *mut Box<ColumnIndices>) {
    if array_ref.is_null() { return }
    unsafe {
        let instance = &mut *array_ref;
        if !instance.indices.is_null() {
            let s = std::slice::from_raw_parts_mut(instance.indices, instance.len as usize) ;
            let s = s.as_mut_ptr();
            drop(Box::from_raw(s));
        }
        drop(Box::from_raw(instance));
    }
}

fn card_vec_to_patience_card_symbols_array_reference(card_vec: Vec<CardSymbols>) -> *mut Box<PatienceCardSymbolsArray> {

        let mut pdp: Vec<PatienceCardSymbols> = card_vec.iter()
            .map(|cs| PatienceCardSymbols::from(cs))
            .collect();
        pdp.shrink_to_fit();
        let array_len = pdp.len();

        let mut boxed_slice = pdp.into_boxed_slice();
        let array = boxed_slice.as_mut_ptr();

        mem::forget(boxed_slice);

        let retval =  Box::new(PatienceCardSymbolsArray {
            array,
            array_len
        });

        Box::into_raw(retval.into())
}

/// Contains the index values of the columns.
#[repr(C)]
pub struct ColumnIndices {
    /// Length of the array
    len: usize,
    /// Pointer is never `null`
    indices: *mut u8
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct PatienceMoveSuggestionArray {
    /// The amount of elements in the array
    array_len: usize,

    /// A reference to the card symbol array.
    /// Use [patience_move_suggestion_from_array] for dereferencing an entry.
    array: *mut PatienceMoveSuggestion
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct PatienceMoveSuggestion {
    pub start: *const c_char,
    pub index: c_uint,
    pub target: *const c_char,
}

impl PatienceMoveSuggestion {
    fn from(ms: &MoveSuggestion) -> PatienceMoveSuggestion {

        let start = CString::new(ms.start.to_string()).unwrap();
        let target: CString = CString::new(ms.target.to_string()).unwrap();

        let retval = PatienceMoveSuggestion {
            start: start.as_ptr(),
            index: match ms.index {
                None => 0,
                Some(v) => v
            },
            target: target.as_ptr(),
        };
        mem::forget(start);
        mem::forget(target);

        retval
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct PatienceCardSymbolsArray {
    /// The amount of elements in the array
    array_len: usize,

    /// A reference to the card symbol array.
    /// Use [patience_card_symbol_from_array] for dereferencing an entry.
    array: *mut PatienceCardSymbols
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct PatienceCardSymbols {
    /// The card symbol as UTF-8 character.
    /// 
    /// Value will be from U+1F0A0 to U+1F0DE for all defined playing cards of Unicode 15.1.0 but not U+1F0BF or U+1F0CF.
    /// 
    /// See [Playing cards in Unicode](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode)
    /// for further information.
    /// 
    /// Never `null`.
    pub card_symbol: *const c_char,

    /// The suit symbol as UTF-8 character.
    /// If the card is _unknown_, the value of a `suit_symbol` is a `null` pointer
    /// 
    /// * `♣` for club
    /// * `♦` for diamond
    /// * `♥` for heart
    /// * `♠` for spade
    pub suit_symbol: *const c_char,

    /// The card value.
    /// 
    /// * `1` for Ace
    /// * `11` for Jack
    /// * `12` for Queen
    /// * `13` for King
    /// * `0` for an _unknown_ card
    pub value: c_uint
}

impl PatienceCardSymbols {
    fn from(cs: &CardSymbols) -> PatienceCardSymbols {

        let pcs = CString::new(cs.card_symbol.to_string()).unwrap();

        let pss: CString = match cs.suit_symbol {
            None => CString::new("").unwrap(),
            Some(s) => CString::new(s.to_string()).unwrap()
        };

        let pss_ref;
        if !pss.is_empty() {
            pss_ref = pss.as_ptr();
        } else {
            pss_ref = ptr::null();
        }

        let retval = PatienceCardSymbols {
            card_symbol: pcs.as_ptr(),
            suit_symbol: pss_ref,
            value: match cs.value {
                None => 0,
                Some(v) => v
            }
        };
        if !pss.is_empty() {
            mem::forget(pss);
        }
        mem::forget(pcs);

        retval
    }
}

#[repr(C)]
pub struct PatienceGame {
    game: Game
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::ffi::CString;

    #[test]
    fn simple_creation() {
        // Given
        let game = patience_create_game();

        // When
        let result = patience_query_talon_len(game);

        // Then
        assert_eq!(result, 24);

        // After
        patience_destroy_game(game);
    }

    #[test]
    fn query_is_finished() {
        // Given
        let game = patience_create_game();

        // When
        let result = patience_query_is_finished(game);

        // Then
        assert!(!result);

        // After
        patience_destroy_game(game);
    }

    #[test]
    fn query_is_solvable() {
        // Given
        let game = patience_create_game();

        // When
        let result = patience_query_is_solvable(game);

        // Then
        assert!(!result);

        // After
        patience_destroy_game(game);
    }

    #[test]
    fn query_column() {
        // Given
        let game = patience_create_game();

        // When
        let result = patience_query_column(game, 4);
        unsafe {
            let card_array = &mut *result;
            assert_eq!(card_array.array_len, 4);

            let card_ref = patience_card_symbol_from_array(card_array, card_array.array_len - 1);
            let card = &mut *card_ref;

            assert_ne!(card.value, 0);
        }

        // After
        patience_destroy_card_symbols_array(result);
        patience_destroy_game(game);
    }

    #[test]
    fn do_move_with_null_start() {
        // Given
        let game = patience_create_game();

        // When
        let result = patience_do_move(game, ptr::null(), 0, ptr::null());

        // Then
        assert_eq!(result, PATIENCE_DO_MOVE_NO_START);

        // After
        patience_destroy_game(game);
    }

    #[test]
    fn do_move_with_null_game_ref() {
        // When
        let result = patience_do_move(ptr::null_mut(), ptr::null(), 0, ptr::null());

        // Then
        assert_eq!(result, PATIENCE_DO_MOVE_NO_GAME_REFERENCE);
    }

    #[test]
    fn do_move_for_discardpile() {
        // Given
        let game = patience_create_game();
        let dp = CString::new("dp").unwrap();
        let dp_ptr = dp.as_ptr();

        // When
        let retval = patience_do_move(game, dp_ptr, 0, ptr::null());

        // Then
        assert_eq!(retval, PATIENCE_DO_MOVE_OK);
        assert_eq!(patience_query_talon_len(game), 23);

        // And when
        let dp = patience_query_discardpile(game, true);
        unsafe {
            let array = &mut *dp;
            assert_eq!(array.array_len, 1);
            let cs_ref = patience_card_symbol_from_array(dp, array.array_len - 1);
            let cs = &mut *cs_ref;

            assert_ne!(cs.value, 0);
            let suit = CStr::from_ptr(cs.suit_symbol as *const i8).to_str().unwrap();
            assert!(suit == "♣" || suit == "♠" || suit == "♥" || suit == "♦");
        }

        // After
        patience_destroy_card_symbols_array(dp);
        patience_destroy_game(game);
    }

    #[test]
    fn query_column_indices() {
        unsafe {
           // Given
            let game = patience_create_game();

            // When
            let indices_ref = patience_query_column_indices(game);

            // Then
            let indices: &mut Box<ColumnIndices> = &mut *indices_ref;
            assert_eq!(indices.len, 7);

            // And Then
            assert_eq!(patience_column_index_from_column_indices(indices_ref, 1), 2);

            // After
            patience_destroy_column_indices(indices_ref)
        }
    }
}
