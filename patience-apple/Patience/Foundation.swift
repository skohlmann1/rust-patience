// Rust Patience Game, Swift Frontend
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
#if os(iOS)
    import UIKit
#endif

let DEVICE = DeviceType().type

fileprivate struct DeviceType {

    var type: Device {
        get {
            #if canImport(UIKit)
            if UIDevice.current.userInterfaceIdiom == .pad {
                return .ipad
            } else if UIDevice.current.userInterfaceIdiom == .phone {
                return .iphone
            }
            #elseif os(macOS)
                return .mac
            #else
                return .unsupported
            #endif
            return .unsupported
        }
    }
}

enum Device {
    case iphone
    case ipad
    case mac
    case unsupported
}
