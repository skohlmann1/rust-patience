//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::vec::*;
use super::KnownTopStack;
use super::Suggestionable;
use super::CardStack;
use super::TopMoveable;
use super::card::*;

#[derive(Debug)]
pub struct Column {
    pub cards: Vec<Card>
}

impl Column {

    pub fn new() -> Column {
        Column {
            cards: Vec::new()
        }
    }

    pub fn is_top_known(&self) -> bool {
        match self.top() {
            Some(card) => card.is_known(),
            None => false
        }
    }

    pub fn make_top_known(&mut self) {

        if !self.is_top_known() {
            if let Some(card) = self.cards.pop() {
                self.cards.push(Card::create_known(card.value, card.suit))
            }
        }
    }

    pub fn count_unknowns(&self) -> u32 {
        *(&self.cards.iter().filter(|card| card.is_unknown()).count()) as u32
    }

    // Needs more tests
    pub fn move_slice(&mut self, index: usize, target: &mut Column) -> Result<(), Option<&str>> {
        if self.cards.len() < index {
            return Err(Some("Index not matching"));
        }
        let range = self.cards.len();
        let slice : Vec<Card> = self.cards.drain(index..range).collect();
        match target.add_slice(slice) {
            Some(back_slice) => {
                self.cards.extend(back_slice);
                Err(Some("Move not possible"))
            },
            None => {
                self.make_top_known();
                Ok(())
            }
        }
    }

    fn add_slice(&mut self, slice: Vec<Card>) -> Option<Vec<Card>> {
        match slice.first() {
            Some(card) => {
                match self.top() {
                    Some(own_card) => {
                        if own_card.value - 1 == card.value && own_card.suit.color() != card.suit.color() {
                            self.cards.extend(slice);
                            return None
                        }
                    },
                    None => {
                        // is empty column. So check if slice first card is king for possible move
                        match slice.first() {
                            Some(first_card) => {
                                if first_card.value == KING {
                                    self.cards.extend(slice);
                                    return None;
                                }
                                ()
                            },
                            _ => ()
                        }
                    }
                }
            },
            _ => ()
        }
        Some(slice)
    }

}

impl KnownTopStack for Column {
    fn top(&self) -> Option<&Card> {
        self.cards.last()
    }
}

impl Suggestionable for Column {
    fn can_suggest(&self, card: &Card) -> bool {
        match self.top() {
            None => card.value == KING,
            Some(top) => top.is_known() && top.value - 1 == card.value && top.suit.color() != card.suit.color()
        }
    }
}

impl CardStack for Column {
    fn push(&mut self, card: Card) -> Result<(), (Card, Option<&str>)> {

        if card.is_unknown() {
            match self.top() {
                Some(top) => {
                    if top.is_known() {
                        return Err((card, None))
                    } 
                },
                None => ()
            }
            self.cards.push(card);
            return Ok(())
        }

        match self.top() {
            // Todo: optimizable
            None => {
                if card.value == KING {
                    self.cards.push(card);
                    return Ok(())
                }
            },
            Some(top) => {
                if top.is_unknown() {
                    self.cards.push(card);
                    return Ok(())
                }
                if top.value - 1 == card.value && top.suit.color() != card.suit.color() {
                    self.cards.push(card);
                    return Ok(())
                }
            }
        }
        Err((card, None))
    }
}

impl TopMoveable for Column {
    
    fn move_top_to<CS: CardStack>(&mut self, card_stack: &mut CS) -> Result<(), Option<&str>> {
        match self.cards.pop() {
            Some(card) => {
                match card_stack.push(card) {
                    Ok(_) => {
                        self.make_top_known();
                        Ok(())
                    },
                    Err((bc, _f)) => {
                        self.cards.push(bc);
                        Err(None)
                    }
                }
            },
            None => Err(None)
        }
    }

}

#[cfg(test)]
pub mod tests {
    use crate::domain::column::*;

    const QUEEN: u32 = 12;
    const JACK: u32= 11;

    #[test]
    fn column_2_column_move_slice_with_matching_template() {
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));
        let _ = source.push(Card::create_known(KING, Suit::Heart));
        let _ = source.push(Card::create_known(QUEEN, Suit::Club));
        let _ = source.push(Card::create_known(JACK, Suit::Diamond));
        assert_eq!(source.cards.len(), 4);

        let target = &mut Column::new();
        let _ = target.push(Card::create_known(KING, Suit::Diamond));
        assert_eq!(target.cards.len(), 1);

        let _ = source.move_slice(2, target);

        assert_eq!(target.top().unwrap().value, JACK);
        assert_eq!(source.top().unwrap().value, KING);

    }

    #[test]
    fn column_2_column_move_slice_with_unmatching_template() {
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));
        let _ = source.push(Card::create_known(KING, Suit::Heart));
        let _ = source.push(Card::create_known(QUEEN, Suit::Club));
        let _ = source.push(Card::create_known(JACK, Suit::Diamond));
        assert_eq!(source.cards.len(), 4);

        let target = &mut Column::new();
        let _ = target.push(Card::create_known(KING, Suit::Diamond));
        assert_eq!(target.cards.len(), 1);

        let _ = source.move_slice(1, target);

        assert_eq!(source.top().unwrap().value, JACK);
        assert_eq!(target.top().unwrap().value, KING);
    }

    #[test]
    fn column_2_column_with_empty_target_and_slice_first_is_king() {
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));
        let _ = source.push(Card::create_known(KING, Suit::Heart));
        let _ = source.push(Card::create_known(QUEEN, Suit::Club));
        let _ = source.push(Card::create_known(JACK, Suit::Diamond));
        assert_eq!(source.cards.len(), 4);

        let target = &mut Column::new();

        let _ = source.move_slice(1, target);

        assert_eq!(source.top().unwrap().value, 3);
        assert_eq!(target.top().unwrap().value, JACK);
        assert_eq!(target.cards.first().unwrap().value, KING);
    }

    #[test]
    fn column_2_column_with_empty_target_and_slice_first_is_queen() {
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));
        let _ = source.push(Card::create_known(QUEEN, Suit::Club));
        let _ = source.push(Card::create_known(JACK, Suit::Diamond));
        assert_eq!(source.cards.len(), 3);

        let target = &mut Column::new();

        let _ = source.move_slice(1, target);

        assert_eq!(source.top().unwrap().value, JACK);
        assert!(target.cards.is_empty());
    }

    #[test]
    fn colum_has_unknown() {
        // Given
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));

        // Then
        assert!(source.has_unknown())
    }

    #[test]
    fn colum_has_unknown_while_empty() {
        // Given
        let source = &mut Column::new();

        // Then
        assert!(!source.has_unknown());
    }

    #[test]
    fn colum_has_only_known() {
        // Given
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));
        source.make_top_known();

        // Then
        assert!(!source.has_unknown());
    }

    #[test]
    fn unknown_count() {
        // Given
        let source = &mut Column::new();
        let _ = source.push(Card::create_unknown(3, Suit::Diamond));
        let _ = source.push(Card::create_unknown(2, Suit::Club));
        let _ = source.push(Card::create_known(3, Suit::Heart));

        // Then
        assert_eq!(source.count_unknowns(), 2);
    }

    #[test]
    fn column_2_queen_heart_top_to_column_1_king_diamond() {
        // Given
        let col1 = &mut Column::new();
        let _ = col1.push(Card::create_known(KING, Suit::Heart));
        let col2 = &mut Column::new();
        let _ = col2.push(Card::create_unknown(QUEEN, Suit::Diamond));
        col2.make_top_known();

        // When
        let res = col2.move_top_to(col1);

        // Then
        assert!(res.is_err());
    }

    #[test]
    fn column_2_queen_heart_slice_to_column_1_king_diamond() {
        // Given
        let col1 = &mut Column::new();
        let _ = col1.push(Card::create_known(KING, Suit::Heart));
        let col2 = &mut Column::new();
        let _ = col2.push(Card::create_unknown(QUEEN, Suit::Diamond));
        col2.make_top_known();

        // When
        let res = col2.move_slice(0, col1);

        // Then
        assert!(res.is_err());
    }

    #[test]
    fn can_suggest_king_to_empty_column() {
        // Given
        let col = &mut Column::new();
        let king = Card::create_known(KING, Suit::Heart);

        // When
        let result = col.can_suggest(&king);

        // Then
        assert_eq!(result, true)
    }

    #[test]
    fn can_suggest_ace_to_empty_column() {
        // Given
        let col = &mut Column::new();
        let ace = Card::create_known(ACE, Suit::Heart);

        // When
        let result = col.can_suggest(&ace);

        // Then
        assert_eq!(result, false);
    }

    #[test]
    fn can_suggest_black_queen_to_column_with_red_king_as_top_card() {
        // Given
        let col = &mut Column::new();
        let king = Card::create_known(KING, Suit::Heart);
        let _ = col.push(king);
        let queen = Card::create_known(QUEEN, Suit::Spade);

        // When
        let result = col.can_suggest(&queen);

        // Then
        assert_eq!(result, true)
    }

    #[test]
    fn can_suggest_red_queen_to_column_with_red_king_as_top_card() {
        // Given
        let col = &mut Column::new();
        let king = Card::create_known(KING, Suit::Heart);
        let _ = col.push(king);
        let queen = Card::create_known(QUEEN, Suit::Diamond);

        // When
        let result = col.can_suggest(&queen);

        // Then
        assert_eq!(result, false)
    }

    #[test]
    fn can_suggest_to_unknown() {
        // Given
        let col = &mut Column::new();
        let king = Card::create_unknown(KING, Suit::Heart);
        let _ = col.push(king);

        let queen = Card::create_known(QUEEN, Suit::Spade);

        // When
        let result = col.can_suggest(&queen);

        // Then
        assert_eq!(result, false)
    }

    impl Column {
        fn has_unknown(&self) -> bool {
            match self.cards.first() {
                None => false,
                Some(card) => card.is_unknown()
            }
        }
    }
}
