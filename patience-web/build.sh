#!/usr/bin/env bash
pdir=$(pwd)
./clean.sh "${pdir}"
mkdir "${pdir}/static"
echo "Copy artwork"
cp -r "../artwork/base deck single" "${pdir}/static/deck1"
cp -r "../artwork/back5.svg" "${pdir}/static"
cp -r "../artwork/empty.svg" "${pdir}/static"
cp -r "../artwork/addcard.svg" "${pdir}/static"
echo "*" > "${pdir}/static/.gitignore"

cd ../wasm-core-binding
./build.sh
cp -r pkg "${pdir}"
cd "${pdir}"
