//  Rust Patience Game
//  Copyright (C) 2024 Sascha Kohlmann
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt;

pub const KING: u32 = 13;
pub const ACE: u32 = 1;

#[derive(PartialEq,Debug)]
pub struct Card {
    known: bool,
    pub value: u32,
    pub suit: Suit,
}

impl Card {

    pub fn symbol(&self) -> char {
        if self.is_unknown() {
            return char::from_u32(0x1F0A0).unwrap()
        }

        let first_card: u32 = 0x1F0A1;
        let offset = (self.value - 1) + self.suit.index() * 16;
        let symbol = first_card + offset;
        char::from_u32(symbol).unwrap()
    }

    #[allow(dead_code)]
    pub fn name(&self) -> String {
        if self.value == 1 {
            return format!("Ace of {}s", self.suit)
        }
        if self.value == 11 {
            return format!("Jack of {}s", self.suit)
        }
        if self.value == 12 {
            return format!("Queen of {}s", self.suit)
        }
        if self.value == 13 {
            return format!("King of {}s", self.suit)
        }

        format!("{} of {}s", self.value, self.suit)
    }

    pub fn is_known(&self) -> bool {
        self.known
    }

    pub fn is_unknown(&self) -> bool {
        !self.is_known()
    }

    pub fn to_known(card: Card) -> Card {
        Card::create_known(card.value, card.suit)
    }

    pub fn to_unknown(card: Card) -> Card {
        Card::create_unknown(card.value, card.suit)
    }

    pub fn create_known(value: u32, suit: Suit) -> Card {
        Card::new(true, value, suit)
    }

    pub fn create_unknown(value: u32, suit: Suit) -> Card {
        Card::new(false, value, suit)
    }

    fn new(known: bool, value: u32, suit: Suit) -> Card {
        if value < 1 || value > 13 {
            panic!("card value must be between 1 and 13. Is {}", value);
        }
        Card { known, value, suit }
    }
}


/// Use the values as offset for symbol calculation.
#[derive(Eq, Hash, PartialEq, Debug, Clone)]
pub enum Suit {
    Spade,
    Heart,
    Diamond,
    Club
}

impl Suit {
    pub fn color(&self) -> Color {
        match self {
            Self::Spade => Color::Black,
            Self::Heart => Color::Red,
            Self::Diamond => Color::Red,
            Self::Club => Color::Black
        }
    }

    #[allow(dead_code)]
    pub fn symbol(&self) -> char {
        match self {
            Self::Spade => char::from_u32(0x2660).unwrap(),
            Self::Heart => char::from_u32(0x2665).unwrap(),
            Self::Diamond => char::from_u32(0x2666).unwrap(),
            Self::Club => char::from_u32(0x2663).unwrap()
        }
    }

    fn index(&self) -> u32 {
        match self {
            Self::Spade => 0,
            Self::Heart => 1,
            Self::Diamond => 2,
            Self::Club => 3
        }
    }

    pub fn of(name: &str) -> Result<Suit, &str> {
        match name.trim_start().chars().next() {
            Some(first) => 
                match first {
                    '♠' | '♤' | 's' | 'S' => Ok(Suit::Spade),
                    '♣' | '♧' | 'c' | 'C' => Ok(Suit::Club),
                    '♦' | '♢' | 'd' | 'D' => Ok(Suit::Diamond),
                    '♥' | '♡' | 'h' | 'H' => Ok(Suit::Heart),
                    _ => Err("Unsupported suit character")
                },
            None => Err("No suit character")
        }
    }
}


impl fmt::Display for Suit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Spade => write!(f, "Spade"),
            Self::Heart => write!(f, "Heart"),
            Self::Diamond => write!(f, "Diamond"),
            Self::Club => write!(f, "Club"),
        }
    }
}

#[derive(PartialEq,Debug)]
pub enum Color {
    Red,
    Black
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Red => write!(f, "red"),
            Self::Black => write!(f, "black"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::card::*;

    #[test]
    fn create_known_card() {
        let card = Card::create_known(1, Suit::Club);

        assert!(card.is_known());
        assert_eq!(card.value, 1);
        assert_eq!(card.suit, Suit::Club);
    }

    #[test]
    fn create_unknown_card() {
        let card = Card::create_unknown(1, Suit::Club);

        assert!(card.is_unknown());
    }

    #[test]
    fn suit_of_club_for_string() {
        assert_eq!(Suit::of("club").unwrap(), Suit::Club);
        assert_eq!(Suit::of("CLUB").unwrap(), Suit::Club);
        assert_eq!(Suit::of("C").unwrap(), Suit::Club);
        assert_eq!(Suit::of("c").unwrap(), Suit::Club);
        assert_eq!(Suit::of("♣").unwrap(), Suit::Club);
        assert_eq!(Suit::of("♧").unwrap(), Suit::Club);
    }

    #[test]
    fn suit_of_spade_for_string() {
        assert_eq!(Suit::of("shape").unwrap(), Suit::Spade);
        assert_eq!(Suit::of("Shape").unwrap(), Suit::Spade);
        assert_eq!(Suit::of("s").unwrap(), Suit::Spade);
        assert_eq!(Suit::of("S").unwrap(), Suit::Spade);
        assert_eq!(Suit::of("♤").unwrap(), Suit::Spade);
        assert_eq!(Suit::of("♠").unwrap(), Suit::Spade);
    }

    #[test]
    fn suit_of_diaond_for_string() {
        assert_eq!(Suit::of("diamond").unwrap(), Suit::Diamond);
        assert_eq!(Suit::of("DIMa").unwrap(), Suit::Diamond);
        assert_eq!(Suit::of("d").unwrap(), Suit::Diamond);
        assert_eq!(Suit::of("D").unwrap(), Suit::Diamond);
        assert_eq!(Suit::of("♦").unwrap(), Suit::Diamond);
        assert_eq!(Suit::of("♢").unwrap(), Suit::Diamond);
    }

    #[test]
    fn suit_of_heart_for_string() {
        assert_eq!(Suit::of("habemus").unwrap(), Suit::Heart);
        assert_eq!(Suit::of("Heart").unwrap(), Suit::Heart);
        assert_eq!(Suit::of("h").unwrap(), Suit::Heart);
        assert_eq!(Suit::of("H").unwrap(), Suit::Heart);
        assert_eq!(Suit::of("♡").unwrap(), Suit::Heart);
        assert_eq!(Suit::of("♥").unwrap(), Suit::Heart);
    }

    #[test]
    fn suit_of_unsupported() {
        assert!(Suit::of("XYZ").is_err());
    }

    #[test]
    fn suit_of_empty() {
        assert!(Suit::of("").is_err());
    }

}