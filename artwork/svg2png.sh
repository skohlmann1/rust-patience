#!/usr/bin/env bash
#
# Generates an PNGs from SVG images.
#
# Parameters
#  b  -  optional - Set the background color to white when "true" (default: false)
#  w  -  required - The width in pixel of the output image.
#                   The tool keeps the aspect ratio.
#  o  -  required - The output file name
#  f  -  optional - The output format (default: png)
#
# Last parameter must be the path to the input file, the SVG image.
#
# Dependencies: "rsvg-convert" must be available.
#
# Under macOS, install “rsvg-convert” e.g. with “brew install librsvg”.

#set -x
set +o nounset



BACKGROUND=false
FORMAT=png
INPUT="${@: -1}"

if [ ! $(type -p "rsvg-convert") ]; then
    echo "rsvg-convert not installed."
    echo "install with \"brew install librsvg\""
    exit 2
fi


while getopts bw:o:f: opt
do
    case "${opt}" in
        w) eval "WIDTH='${OPTARG}'";;
        b) eval "BACKGROUND=true";;
        o) eval "OUTPUT='${OPTARG}'";;
        f) eval "FORMAT='${OPTARG}'";;
    esac
done

if [ -z "${OUTPUT+x}" ] && [ "${#OUTPUT}" -eq 0 ] ; then
    OUTPUT="${INPUT%.*}.${FORMAT}"
fi


if "${BACKGROUND}"; then
    if [ -z "${WIDTH+x}" ] && [ "${#WIDTH}" -eq 0 ] ; then
        rsvg-convert --keep-aspect-ratio --format ${FORMAT} --output "${OUTPUT}" --background-color white "${INPUT}"
    else
        rsvg-convert --keep-aspect-ratio --width ${WIDTH} --format ${FORMAT} --output "${OUTPUT}" --background-color white "${INPUT}"
    fi
else
    if [ -z "${WIDTH+x}" ] && [ "${#WIDTH}" -eq 0 ] ; then
        rsvg-convert --keep-aspect-ratio --format ${FORMAT} --output "${OUTPUT}" "${INPUT}"
    else
        rsvg-convert --keep-aspect-ratio --width ${WIDTH} --format ${FORMAT} --output "${OUTPUT}" "${INPUT}"
    fi
fi


set -o nounset
#set +x
