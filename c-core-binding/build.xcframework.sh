#!/usr/bin/env bash
#
# Generates an Apple XCFramework.
#
# Parameters (all optional)
#  r  -  optional - release build (default: debug)
#  s  -  optional - sign the build (defaul: no sign)
#
#
# To sign your code an identity must be provided.
# To get you code sign identity please execute
#
#     security find-identity -v -p codesigning
#
# where the first part of  the output, something like
# 348ADF83438484388493 (shorter or longer) is your codesign
# identity, which is passed as an argument to the --sign flag
# of 'codesign' tool.
#
# As it is not a good idea to put your identity into the code
# which is stored within a repository, set the identity into
# the environment variable APPLE_PATIENCE_SIGN_IDENTITY
#
# 'codesign' may ask you to enter your keychain password.
#
# Exit values:
#
#  0  -  everything is ok
#  1  -  missign required tool like cargo or xcodebuild...
#  2  -  framework build failed
#  3  -  signing failed but signing tool is available

set -o nounset

function source_dir {
    local SOURCE="${BASH_SOURCE[0]}"
    DIR="$( dirname "$SOURCE" )"
    while [ -h "$SOURCE" ]; do
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
        DIR="$( cd -P "$( dirname "$SOURCE"  )" && pwd )"
    done
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

source_dir

SHOULD_SIGN=false
RELEASE=false

while getopts sr opt; do
    case "${opt}" in
        s) eval "SHOULD_SIGN=true";;
        r) eval "RELEASE=true";;
    esac
done


DEPENDENCIES=("cargo" "xcodebuild")

for dep in "${DEPENDENCIES[@]}"; do
    if [ ! $(type -p "${dep}") ]; then
        echo "'${dep}' not installed."
        echo "Give up"
        exit 1
    fi
done

build_dir="${DIR}/target"
base_project_target="${DIR}/../target"
framework_name="RustPatience.xcframework"
framework_dir="./target/${framework_name}"
target=debug
if "${RELEASE}" ; then
    target="release"
fi

set -o nounset

TARGETS=("aarch64-apple-darwin" "aarch64-apple-ios" "x86_64-apple-darwin" "aarch64-apple-ios-sim")

echo "build ${target}"
rm -rf "${framework_dir}"
mkdir -p "${build_dir}"

for t in "${TARGETS[@]}"; do
    rustup target add "${t}"
    if [[ "${target}" == "release" ]]; then
        cargo build --target "${t}" --release
    else 
        cargo build --target "${t}"
    fi

    cp "${base_project_target}/${t}/${target}/libc_patience_core_binding.a" "${build_dir}/${t}.a"
    mkdir "${build_dir}/${t}"
    cp "${base_project_target}/${t}/${target}/libc_patience_core_binding.h" "${build_dir}/${t}/${t}.h"
done

rm -rf "${base_project_target}/${framework_name}"
xcodebuild -create-xcframework -library "${build_dir}/${TARGETS[0]}.a" -headers "${build_dir}/${TARGETS[0]}" \
                               -library "${build_dir}/${TARGETS[1]}.a" -headers "${build_dir}/${TARGETS[1]}" \
                               -library "${build_dir}/${TARGETS[3]}.a" -headers "${build_dir}/${TARGETS[3]}" \
                               -allow-internal-distribution \
                               -output "${build_dir}/${framework_name}"
if [ "${?}" -ne 0 ]; then
    echo "XCFramework build failed"
    exit 2
fi

cat ./module.modulemap.template | sed "s/{REPLACE_HEADER_FILE_NAME}/${TARGETS[0]}.h/g" > "${build_dir}/${framework_name}/macos-arm64/Headers/module.modulemap"
cat ./module.modulemap.template | sed "s/{REPLACE_HEADER_FILE_NAME}/${TARGETS[1]}.h/g" > "${build_dir}/${framework_name}/ios-arm64/Headers/module.modulemap"
cat ./module.modulemap.template | sed "s/{REPLACE_HEADER_FILE_NAME}/${TARGETS[3]}.h/g" > "${build_dir}/${framework_name}/ios-arm64-simulator/Headers/module.modulemap"

if "${SHOULD_SIGN}"; then
    if [ $(type -p "codesign") ]; then
        codesign --timestamp -s "${APPLE_PATIENCE_SIGN_IDENTITY}" "${build_dir}/${framework_name}"
        sign_result="${?}"
        if [ "${sign_result}" -ne 0 ]; then
            echo "XCFramework signing failed with ${sign_result}"
            exit 3
        fi        
    else
        echo "codesign command required but not found."
        echo "Give up"
        exit 1
    fi

fi

rm -rf "${build_dir}/${framework_name}.zip"
cd "${build_dir}/${framework_name}"
zip -0 -r "${build_dir}/${framework_name}.zip" .
cd -

cp -r "${build_dir}/${framework_name}" "${base_project_target}"
cp "${build_dir}/${framework_name}.zip" "${base_project_target}"
