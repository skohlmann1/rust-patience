// Rust Patience Game, Swift Frontend
// Copyright (C) 2024 Sascha Kohlmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import SwiftUI
import PatienceFoundation

struct Playfield : View {
    
    @Environment(PatienceModel.self) private var model
    
    var body: some View {
        GeometryReader { geometryProxy in
            
            let tableau = StackFoundation(
                card: Card(width: Playfield.cardSize(geometryProxy.size)),
                knownOverlayPercentage: Playfield.overlayKnownPercentage(geometryProxy.size)
            )
            let zero = StackFoundation(
                card: Card(width: Playfield.cardSize(geometryProxy.size)),
                knownOverlayPercentage: 0,
                unknownOverlayPercentage: 0
            )
            
            if Playfield.portrait(geometryProxy.size) {
                // Portrait for all Devices
                VStack(alignment: .center) {
                    Grid(alignment: .top, horizontalSpacing: tableau.padding, verticalSpacing: tableau.padding) {
                        GridRow(alignment: .top) {
                            Group {
                                CardStack(stackName: "s1", emptyStack: .add, foundation: zero, cards: model.suit1)
                                CardStack(stackName: "s2", emptyStack: .add, foundation: zero, cards: model.suit2)
                                CardStack(stackName: "s3", emptyStack: .add, foundation: zero, cards: model.suit3)
                            }
                            CardStack(stackName: "s4", emptyStack: .add, foundation: zero, cards: model.suit4)
                            Color.clear.gridCellUnsizedAxes([.horizontal, .vertical])
                            CardStack(stackName: "dp", emptyStack: .add, foundation: zero, cards: model.discardPile)
                            CardStack(stackName: "st", emptyStack: .back, foundation: zero, cards: [])
                                .onHover { hovered in
                                    setCursor(hovered)
                                }.onTapGesture {
                                    self.model.doMove(start: "dp", index: nil, target: nil)
                                }

                        }
                        GridRow(alignment: .top) {
                            Group {
                                CardStack(stackName: "c1", emptyStack: .add, foundation: tableau, cards: model.column1)
                                CardStack(stackName: "c2", emptyStack: .add, foundation: tableau, cards: model.column2)
                                CardStack(stackName: "c3", emptyStack: .add, foundation: tableau, cards: model.column3)
                                CardStack(stackName: "c4", emptyStack: .add, foundation: tableau, cards: model.column4)
                                CardStack(stackName: "c5", emptyStack: .add, foundation: tableau, cards: model.column5)
                                CardStack(stackName: "c6", emptyStack: .add, foundation: tableau, cards: model.column6)
                            }
                            CardStack(stackName: "c7", emptyStack: .add, foundation: tableau, cards: model.column7)
                        }
                    }
                    Text(self.model.msg).foregroundColor(Color.red)
                }
            } else {
                
                if DEVICE == .iphone {
                    // Landscape for iPhone
                    HStack(alignment: .top) {
                        VStack(alignment: .center) {
                            Group {
                                CardStack(stackName: "s1", emptyStack: .add, foundation: zero, cards: model.suit1)
                                CardStack(stackName: "s2", emptyStack: .add, foundation: zero, cards: model.suit2)
                                CardStack(stackName: "s3", emptyStack: .add, foundation: zero, cards: model.suit3)
                            }
                            CardStack(stackName: "s4", emptyStack: .add, foundation: zero, cards: model.suit4)
                        }
                        HStack(alignment: .top, spacing: tableau.padding) {
                            Group {
                                CardStack(stackName: "c1", emptyStack: .add, foundation: tableau, cards: model.column1)
                                CardStack(stackName: "c2", emptyStack: .add, foundation: tableau, cards: model.column2)
                                CardStack(stackName: "c3", emptyStack: .add, foundation: tableau, cards: model.column3)
                                CardStack(stackName: "c4", emptyStack: .add, foundation: tableau, cards: model.column4)
                                CardStack(stackName: "c5", emptyStack: .add, foundation: tableau, cards: model.column5)
                                CardStack(stackName: "c6", emptyStack: .add, foundation: tableau, cards: model.column6)
                            }
                            CardStack(stackName: "c7", emptyStack: .add, foundation: tableau, cards: model.column7)
                        }
                        VStack(alignment: .center) {
                            CardStack(stackName: "st", emptyStack: .back, foundation: zero, cards: [])
                                .onHover { hovered in
                                    setCursor(hovered)
                                }.onTapGesture {
                                    self.model.doMove(start: "dp", index: nil, target: nil)
                                }
                            CardStack(stackName: "dp", emptyStack: .add, foundation: zero, cards: model.discardPile)

                        }
                    }
                    Text(self.model.msg).foregroundColor(Color.red)

                } else {
                    // Landscape for iPad and macOS
                    HStack(alignment: .top) {
                        Grid {
                            GridRow(alignment: .top) {
                                Color.clear.gridCellUnsizedAxes([.horizontal, .vertical])
                                Group {
                                    CardStack(stackName: "s1", emptyStack: .add, foundation: zero, cards: model.suit1)
                                    CardStack(stackName: "s2", emptyStack: .add, foundation: zero, cards: model.suit2)
                                    CardStack(stackName: "s3", emptyStack: .add, foundation: zero, cards: model.suit3)
                                }
                                CardStack(stackName: "s4", emptyStack: .add, foundation: zero, cards: model.suit4)
                            }
                            GridRow(alignment: .top) {
                                Group {
                                    CardStack(stackName: "c1", emptyStack: .add, foundation: tableau, cards: model.column1)
                                    CardStack(stackName: "c2", emptyStack: .add, foundation: tableau, cards: model.column2)
                                    CardStack(stackName: "c3", emptyStack: .add, foundation: tableau, cards: model.column3)
                                    CardStack(stackName: "c4", emptyStack: .add, foundation: tableau, cards: model.column4)
                                    CardStack(stackName: "c5", emptyStack: .add, foundation: tableau, cards: model.column5)
                                    CardStack(stackName: "c6", emptyStack: .add, foundation: tableau, cards: model.column6)
                                }
                                CardStack(stackName: "c7", emptyStack: .add, foundation: tableau, cards: model.column7)
                            }
                        }
                        VStack {
                            CardStack(stackName: "st", emptyStack: .back, foundation: zero, cards: [])
                                .onHover { hovered in
                                    DispatchQueue.main.async {
                                        setCursor(hovered)
                                    }
                                }.onTapGesture {
                                    self.model.doMove(start: "dp", index: nil, target: nil)
                                }
                            CardStack(stackName: "dp", emptyStack: .add, foundation: zero, cards: model.discardPile)
                        }
                    }
                    Text(self.model.msg).foregroundColor(Color.red)

                }
            }
        }
    }
    
    private static func rightSize(_ size: CGSize) -> CGFloat {
        return portrait(size) ? size.width * 0.0 : DEVICE == .iphone ? size.width * 0.3 : size.width * 0.25
    }

    private static func columnSize(_ size: CGSize) -> CGFloat {
        return size.width - rightSize(size)
    }

    private static func sizePerColumn(_ size: CGSize) -> CGFloat {
        return columnSize(size) / 7
    }

    private static func cardSize(_ size: CGSize) -> CGFloat {
        return sizePerColumn(size) - columnPadding(size)
    }

    private static func columnPadding(_ size: CGSize) -> CGFloat {
        var colsize = sizePerColumn(size)
        colsize = colsize - colsize / 105 * 100
        return colsize
    }

    private static func cardHeight(_ size: CGSize) -> CGFloat {
        return cardSize(size) * 1.25
    }

    private static func overlayKnownCard(_ size: CGSize) -> CGFloat {
        return portrait(size) ? cardHeight(size) * 0.45 : cardHeight(size) * 0.25
    }

    private static func overlayUnkownCard(_ size: CGSize) -> CGFloat {
        return cardHeight(size) * 0.10
    }

    private static func maxRequiredHeight(_ size: CGSize) -> CGFloat {
        return 2 * cardHeight(size) + 6 * overlayUnkownCard(size) + 12 * overlayKnownCard(size) + cardHeight(size) * 0.1
    }
    
    private static func overlayKnownPercentage(_ size: CGSize) -> Double {
        portrait(size) ? 45 : 25
    }
    private static func overlayUnknownPercentage(_ size: CGSize) -> Double {
        10
    }

    private static func portrait(_ size: CGSize) -> Bool {
        return size.width < size.height
    }

    private static func landscape(_ size: CGSize) -> Bool {
        return !portrait(size)
    }
    
    private static func device(_ size: CGSize) -> Device {
        DEVICE
    }

    private func setCursor(_ toggle: Bool) {
#if os(macOS)
        DispatchQueue.main.async {
            if toggle {
                NSCursor.pointingHand.push()
            } else {
                NSCursor.pop()
            }
        }
#endif
    }

}

#Preview {
    Playfield()
}
